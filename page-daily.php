<?php

	logger('Loaded the daily weather forecast.');



	echo '<section id="daily">';

		echo '<div class="loop">';
			$i = 0;
			foreach($loop_daily AS $daily) {
				$i++;

				if($i != 1) {
					$timestamp = $daily->timestamp;
					$date_weekday = $daily->date->details->day->weekday->full;
					$weather_d_id = $daily->weather->id;
					$weather_d_idicon = $daily->weather->id_icon;
					$weather_d_icon = $daily->weather->icon;
					$weather_d_clouds = $daily->clouds;
					$weather_d_humidity  = $daily->humidity;
					$weather_d_h2okgair  = $daily->h2o_per_kg_of_air->value;
					$weather_d_uvindex = $daily->uv_index->grade;
					$weather_d_uvindex_colour = $daily->uv_index->colour;
					$weather_d_precip_rain = $daily->weather->rain->mm;
					$weather_d_precip_snow = $daily->weather->snow->mm;

					$weather_d_pressure_hpa = $daily->pressure->hpa;
					$weather_d_pressure_pa = $daily->pressure->pa;
					$weather_d_pressure_bar = $daily->pressure->bar;
					$weather_d_pressure_mmhg = $daily->pressure->mmhg;
					$weather_d_pressure_inhg = $daily->pressure->inhg;
					$weather_d_pressure_psi = $daily->pressure->psi;
					$weather_d_pressure_atm = $daily->pressure->atm;

					$weather_d_wind_ms = $daily->wind->speed->ms->wind;
					$weather_d_wind_kmh = $daily->wind->speed->kmh->wind;
					$weather_d_wind_mph = $daily->wind->speed->mph->wind;
					$weather_d_wind_kt = $daily->wind->speed->knot->wind;
					$weather_d_wind_beaufort = $daily->wind->beaufort;

					$weather_d_precip_rain_mm = $daily->weather->rain->mm;
					$weather_d_precip_rain_cm = $daily->weather->rain->cm;
					$weather_d_precip_rain_in = $daily->weather->rain->in;

					$weather_d_precip_snow_mm = $daily->weather->snow->mm;
					$weather_d_precip_snow_cm = $daily->weather->snow->cm;
					$weather_d_precip_snow_in = $daily->weather->snow->in;

					$astronomy_d_sun_rise_twentyfour = $daily->astronomy->sun->rise->time->hours->twentyfour;
					$astronomy_d_sun_rise_twelve = $daily->astronomy->sun->rise->time->hours->twelve;
					$astronomy_d_sun_rise_minutes = $daily->astronomy->sun->rise->time->minutes;
					$astronomy_d_sun_rise_ampm = $daily->astronomy->sun->rise->time->ampm;
					$astronomy_d_sun_set_twentyfour = $daily->astronomy->sun->set->time->hours->twentyfour;
					$astronomy_d_sun_set_twelve = $daily->astronomy->sun->set->time->hours->twelve;
					$astronomy_d_sun_set_minutes = $daily->astronomy->sun->set->time->minutes;
					$astronomy_d_sun_set_ampm = $daily->astronomy->sun->set->time->ampm;

					$arr_timeformat_rise = [
						0 => $astronomy_d_sun_rise_twelve.':'.(($astronomy_d_sun_rise_minutes < 10 OR $astronomy_d_sun_rise_minutes == 0) ? '0' : null) . $astronomy_d_sun_rise_minutes.' '.$astronomy_d_sun_rise_ampm,
						1 => (($astronomy_d_sun_rise_twentyfour < 10 OR $astronomy_d_sun_rise_twentyfour == 0) ? '0' : null) . $astronomy_d_sun_rise_twentyfour.':'.(($astronomy_d_sun_rise_minutes < 10 OR $astronomy_d_sun_rise_minutes == 0) ? '0' : null) . $astronomy_d_sun_rise_minutes
					];

					$arr_timeformat_set = [
						0 => $astronomy_d_sun_set_twelve.':'.(($astronomy_d_sun_set_minutes < 10 OR $astronomy_d_sun_set_minutes == 0) ? '0' : null) . $astronomy_d_sun_set_minutes.' '.$astronomy_d_sun_set_ampm,
						1 => (($astronomy_d_sun_set_twentyfour < 10 OR $astronomy_d_sun_set_twentyfour == 0) ? '0' : null) . $astronomy_d_sun_set_twentyfour.':'.(($astronomy_d_sun_set_minutes < 10 OR $astronomy_d_sun_set_minutes == 0) ? '0' : null) . $astronomy_d_sun_set_minutes
					];



					$arr_temperature = [
						'min' => [
							0 => $daily->temperatures->min->celcius,
							1 => $daily->temperatures->min->fahrenheit,
							2 => $daily->temperatures->min->kelvin
						],
						'max' => [
							0 => $daily->temperatures->max->celcius,
							1 => $daily->temperatures->max->fahrenheit,
							2 => $daily->temperatures->max->kelvin
						],
						'morning' => [
							0 => $daily->temperatures->morning->celcius,
							1 => $daily->temperatures->morning->fahrenheit,
							2 => $daily->temperatures->morning->kelvin
						],
						'day' => [
							0 => $daily->temperatures->day->celcius,
							1 => $daily->temperatures->day->fahrenheit,
							2 => $daily->temperatures->day->kelvin
						],
						'evening' => [
							0 => $daily->temperatures->evening->celcius,
							1 => $daily->temperatures->evening->fahrenheit,
							2 => $daily->temperatures->evening->kelvin
						],
						'night' => [
							0 => $daily->temperatures->night->celcius,
							1 => $daily->temperatures->night->fahrenheit,
							2 => $daily->temperatures->night->kelvin
						]
					];

					$arr_wind_speed = [
						0 => $weather_d_wind_ms,
						1 => $weather_d_wind_kmh,
						2 => $weather_d_wind_mph,
						3 => $weather_d_wind_kt
					];

					$arr_pressure = [
						0 => format_number($weather_d_pressure_hpa, 0, $delimiter),
						1 => format_number($weather_d_pressure_pa, 3, $delimiter),
						2 => format_number($weather_d_pressure_bar, 3, $delimiter),
						3 => format_number($weather_d_pressure_mmhg, 0, $delimiter),
						4 => format_number($weather_d_pressure_inhg, 2, $delimiter),
						5 => format_number($weather_d_pressure_psi, 3, $delimiter),
						6 => format_number($weather_d_pressure_atm, 3, $delimiter)
					];

					$arr_precipitation_rain = [
						0 => $weather_d_precip_rain_mm,
						1 => $weather_d_precip_rain_cm,
						2 => $weather_d_precip_rain_in
					];

					$arr_precipitation_snow = [
						0 => $weather_d_precip_snow_mm,
						1 => $weather_d_precip_snow_cm,
						2 => $weather_d_precip_snow_in
					];



					echo '<div id="'.$timestamp.'">';
						echo '<div class="happens">';
							echo '<div class="day">';
								echo '<a href="#'.$timestamp.'">';
									echo humanreadable_days($timestamp, $place_timezone);
								echo '</a>';
							echo '</div>';

							echo '<div class="date">'.date_($timestamp, 'month-day').'</div>';
						echo '</div>';

						echo '<div class="weather">';
							echo weather($weather_d_idicon . $weather_d_id);
						echo '</div>';

						echo '<i class="'.$weather_d_icon.'"></i>';


						echo '<div class="temperatures">';
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang->words->lowest.':';
								echo '</div>';

								echo '<div class="value">';
									echo format_number($arr_temperature['min'][$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
								echo '</div>';
							echo '</div>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang->words->highest.':';
								echo '</div>';

								echo '<div class="value">';
									echo format_number($arr_temperature['max'][$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
								echo '</div>';
							echo '</div>';
						echo '</div>';



						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->temperature->actual.' ('.($get_timeformat == 0 ? '6 AM' : '06:00').')';
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_temperature['morning'][$get_temperature], 1, $delimiter).' '.get_type('temperature', $get_temperature);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->temperature->actual.' ('.($get_timeformat == 0 ? '12 PM' : '12:00').')';
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_temperature['day'][$get_temperature], 1, $delimiter).' '.get_type('temperature', $get_temperature);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->temperature->actual.' ('.($get_timeformat == 0 ? '6 PM' : '18:00').')';
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_temperature['evening'][$get_temperature], 1, $delimiter).' '.get_type('temperature', $get_temperature);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->temperature->actual.' ('.($get_timeformat == 0 ? '12 AM' : '00:00').')';
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_temperature['night'][$get_temperature], 1, $delimiter).' '.get_type('temperature', $get_temperature);
							echo '</div>';
						echo '</div>';



						echo '<div class="space"></div>';



						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->wind->speed;
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_wind_speed[$get_windspeed], 1, $delimiter).' '.get_type('windspeed', $get_windspeed);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->wind->beaufort;
							echo '</div>';

							echo '<div class="value">';
								echo $weather_d_wind_beaufort;
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->clouds;
							echo '</div>';

							echo '<div class="value">';
								echo $weather_d_clouds.'%';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->humidity;
							echo '</div>';

							echo '<div class="value">';
								echo $weather_d_humidity.'%';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->h2o_kg_air;
							echo '</div>';

							echo '<div class="value">';
								echo format_number($weather_d_h2okgair, 1, $delimiter).' '.mb_strtolower($lang->words->grams);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->pressure;
							echo '</div>';

							echo '<div class="value">';
								echo $arr_pressure[$get_pressure].' '.get_type('pressure', $get_pressure);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->uvindex;
							echo '</div>';

							echo '<div class="value">';
								echo $weather_d_uvindex.'<div class="dot colorbg-'.$weather_d_uvindex_colour.'"></div>';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->precipitation->rain->short;
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_precipitation_rain[$get_precipitation], 2, $delimiter).' '.get_type('precipitation', $get_precipitation);
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->precipitation->snow->short;
							echo '</div>';

							echo '<div class="value">';
								echo format_number($arr_precipitation_snow[$get_precipitation], 2, $delimiter).' '.get_type('precipitation', $get_precipitation);
							echo '</div>';
						echo '</div>';



						echo '<div class="space"></div>';



						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->sunrise;
							echo '</div>';

							echo '<div class="value">';
								echo $arr_timeformat_rise[$get_timeformat];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang->words->sunset;
							echo '</div>';

							echo '<div class="value">';
								echo $arr_timeformat_set[$get_timeformat];
							echo '</div>';
						echo '</div>';
					echo '</div>';
				}
			}
		echo '</div>';



		echo '<div class="charts">';

			echo '<div class="temp">';
				echo '<h4>'.$lang->words->temperature->day.' (°'.get_type('temperature', $get_temperature).')</h4>';

				echo '<div>';
					echo '<div class="legend temp">';
						echo '<div>'.legend_temp(60, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(40, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(20, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(0, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-20, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-40, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-60, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-80, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-100, get_type('temperature', $get_temperature)).'</div>';
					echo '</div>';

					$it = 0;
					foreach($loop_daily AS $daily) {
						$it++;

						if($it != 1) {
							$timestamp = (date('j', $daily->timestamp) < 10 ? '<span class="no-select transparent">0</span>' : null) . date('j/n', $daily->timestamp);
							$arr_temperature = [
								0 => $daily->temperatures->day->celcius,
								1 => $daily->temperatures->day->fahrenheit,
								2 => $daily->temperatures->day->kelvin
							];

							echo '<div class="chart temp">';
								echo '<a href="#'.$daily->timestamp.'" data-tooltip="'.format_number($arr_temperature[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature).'">';
									echo '<div class="bars">';
										echo '<div class="chart-bar plus bar-'.$it.'">';
											echo '<div class="bar" style="height: '.(strpos($arr_temperature[$get_temperature], '-') !== false ? 0 : css_temp($arr_temperature[$get_temperature], get_type('temperature', $get_temperature))).';"></div>';
										echo '</div>';

										echo '<div class="chart-bar minus bar-'.$it.'">';
											echo '<div class="bar" style="height: '.(strpos($arr_temperature[$get_temperature], '-') === false ? 0 : css_temp($arr_temperature[$get_temperature], get_type('temperature', $get_temperature))).';"></div>';
										echo '</div>';
									echo '</div>';

									echo '<div class="label">';
										echo $timestamp;
									echo '</div>';
								echo '</a>';
							echo '</div>';
						}
					}
				echo '</div>';
			echo '</div>';


			echo '<div>';
				echo '<h4>'.$lang->words->precipitation->rain->full.' ('.get_type('precipitation', $get_precipitation).')</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ipr = 0;
					foreach($loop_daily AS $daily) {
						$ipr++;

						if($ipr != 1) {
							$timestamp = (date('j', $daily->timestamp) < 10 ? '<span class="no-select transparent">0</span>' : null) . date('j/n', $daily->timestamp);
							$arr_precipitation_rain = [
								0 => $daily->weather->rain->mm,
								1 => $daily->weather->rain->cm,
								2 => $daily->weather->rain->in
							];


							echo '<div class="chart">';
								echo '<a href="#'.$daily->timestamp.'" data-tooltip="'.format_number($arr_precipitation_rain[$get_precipitation], 1, $delimiter).' '.get_type('precipitation', $get_precipitation).'">';
									echo '<div class="chart-bar bar-'.$ipr.'">';
										echo '<div class="bar" style="height: calc('.$arr_precipitation_rain[$get_precipitation].'% * 5);"></div>';
									echo '</div>';

									echo '<div class="label">';
										echo $timestamp;
									echo '</div>';
								echo '</a>';
							echo '</div>';
						}
					}
				echo '</div>';
			echo '</div>';


			echo '<div>';
				echo '<h4>'.$lang->words->precipitation->snow->full.' ('.get_type('precipitation', $get_precipitation).')</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ips = 0;
					foreach($loop_daily AS $daily) {
						$ips++;

						if($ips != 1) {
							$timestamp = (date('j', $daily->timestamp) < 10 ? '<span class="no-select transparent">0</span>' : null) . date('j/n', $daily->timestamp);
							$arr_precipitation_snow = [
								0 => $daily->weather->snow->mm,
								1 => $daily->weather->snow->cm,
								2 => $daily->weather->snow->in
							];


							echo '<div class="chart">';
								echo '<a href="#'.$daily->timestamp.'" data-tooltip="'.format_number($arr_precipitation_snow[$get_precipitation], 1, $delimiter).' '.get_type('precipitation', $get_precipitation).'">';
									echo '<div class="chart-bar bar-'.$ipr.'">';
										echo '<div class="bar" style="height: calc('.$arr_precipitation_snow[$get_precipitation].'% * 5);"></div>';
									echo '</div>';

									echo '<div class="label">';
										echo $timestamp;
									echo '</div>';
								echo '</a>';
							echo '</div>';
						}
					}
				echo '</div>';
			echo '</div>';

		echo '</div>';

	echo '</section>';

?>
