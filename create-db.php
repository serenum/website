<?php

	require_once 'site-settings.php';



	if($config->save_to_db->enabled == false) {
		die('Database support not enabled.');

	if($db_host == null OR $db_name == null OR $db_username == null OR $db_password == null) {
		die('Please enter all database info in site-settings.php.');

	} else {
		if(empty($settings_encryption_key) OR empty($settings_encryption_iv)) {
			die('Please enter your encryption keys before continuing.');


		} else {
			if($database_exists == 0) {
				sql("CREATE DATABASE IF NOT EXISTS ".$db_name, Array());

				die('Adding the database - done.<br><br>Update the page to create the table.');


			} elseif($table_exists == 0) {
				sql("CREATE TABLE IF NOT EXISTS saved(
						id INT AUTO_INCREMENT UNIQUE KEY,
						data_password VARCHAR(32) NOT NULL COMMENT 'The hashed password.',
						settings_coordinates TEXT NOT NULL,
						settings_language TEXT NOT NULL,
						settings_options TEXT NOT NULL,
						settings_apikey TEXT NULL,
						timestamp_created TEXT NOT NULL COMMENT 'When the settings were created.',
						timestamp_accessed TEXT NULL COMMENT 'When the settings were last accessed by someone with the password.'
					)
					", Array());

				die('Adding the table - done.');


			} else {
				die('Everything looks fine - ignoring.');
			}
		}
	}

?>