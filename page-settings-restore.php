<?php

	require_once 'site-header.php';



	$get_method = (!isset($_GET['met']) ? null : strip_tags(htmlspecialchars($_GET['met'])));
	$get_errorcode = (!isset($_GET['err']) ? null : strip_tags(htmlspecialchars($_GET['err'])));

	$errors = [
		'db' => [
			1 => $lang->messages->errors->no_password,
			2 => $lang->messages->errors->wrong_password
		],
		'json' => [
			1 => $lang->messages->errors->no_file,
			2 => $lang->messages->errors->file_not_uploaded,
			3 => $lang->messages->errors->file_too_big,
			4 => $lang->messages->errors->file_format_invalid,
			5 => $lang->messages->errors->file_not_json
		]
	];

	logger('Loaded the settings restore page.');







	echo '<section id="restore-settings">';
		echo '<h1>'.$lang->pages->settings->restore->title.'</h1>';


		if($config->save_to_db->enabled != false OR $table_exists != 0) {
			echo '<div class="content database">';
				echo '<div><div class="icon upload-database"></div></div>';

				echo '<div>';
					foreach($lang->pages->settings->restore->database->content AS $database) {
						echo (in_array(substr($database, 0, 2), $arr_beginswith) ? $database : $Parsedown->text($database));
					}

					if(isset($_GET['err']) AND $get_method == 'db') {
						logger('Were not able to proceed with the restoration.', 'error', [
							'message' => $errors['db'][$get_errorcode]
						]);

						echo '<div class="message error">';
							echo svgicon('error') . $errors['db'][$get_errorcode];
						echo '</div>';
					}

					echo '<form action="'.url('forms/settings-restore.php?met=db').'" method="POST" autocomplete="off">';
						echo '<input type="text" name="field-password" placeholder="'.$lang->words->password.'" tabindex="1">';
						echo '<input type="submit" name="button-restore" value="'.$lang->words->restore.'" tabindex="2">';
					echo '</form>';
				echo '</div>';
			echo '</div>';
		}


		echo '<div class="content json">';
			echo '<div><div class="icon upload-json"></div></div>';

			echo '<div>';
				foreach($lang->pages->settings->restore->json->content AS $json) {
					echo (in_array(substr($json, 0, 2), $arr_beginswith) ? $json : $Parsedown->text($json));
				}

				if(isset($_GET['err']) AND $get_method == 'json') {
					logger('Were not able to proceed with the restoration.', 'error', [
						'message' => $errors['json'][$get_errorcode]
					]);

					echo '<div class="message error">';
						echo svgicon('error') . $errors['json'][$get_errorcode];
					echo '</div>';
				}

				echo '<form action="'.url('forms/settings-restore.php?met=json').'" method="POST" enctype="multipart/form-data" autocomplete="off">';
					echo '<input type="file" id="choose-file" name="file" accept=".json" tabindex="3">';
					echo '<label for="choose-file">';
						echo $lang->pages->settings->restore->json->choose_file;
					echo '</label>';

					echo '<input type="submit" name="button-restore" value="'.$lang->words->restore.'" tabindex="4">';
				echo '</form>';
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
