<?php

	require_once 'site-header.php';



	$arr_parameters = [
		[
			'code' => ['apikey'],
			'desc' => 'The entered API key for OpenWeatherMap API (if given).'
		]
	];







	echo '<section id="api">';
		echo '<h1>API</h1>';

		echo '<p>Serenum API is a weather based API that uses '.link_('OpenWeatherMap API', 'https://openweathermap.org/api').'. With the API, you\'ll get the following:</p>';

		echo '<ul>';
			echo '<li>Place\'s timezone</li>';
			echo '<li>Current date and time</li>';
			echo '<li>Place details (e.g. city, country)</li>';
			echo '<li>Current weather</li>';
			echo '<li>Minutely forecast for the next hour</li>';
			echo '<li>48 hours forecast</li>';
			echo '<li>7 days forecast</li>';
			echo '<li>Astronomical calculations</li>';
		echo '</ul>';
	echo '</section>';







	require_once 'site-footer.php';

?>
