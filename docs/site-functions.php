<?php

	function url($string, $pure = true) {
		global $is_locally, $get_url, $host;

		$basename_dir = basename(__DIR__) . (($pure == false AND $string == '') ? '' : '/');
		return '/'.($host == 'serenum.org' ? '' : 'serenum/') . ($is_locally == true ? $basename_dir : basename(__DIR__).'/') . ($pure == true ? $string : (empty($string) ? '' : $string) . ($get_url == null ? null : (strpos($string, '?') !== false ? '&' : '?')) . ltrim($get_url, '?'));
	}



	function link_($string, $link) {
		return '<a href="'.$link.'" target="_blank" rel="noopener noreferrer">'.$string.'</a>';
	}

?>