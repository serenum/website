<?php

	require_once '../vendor/autoload.php';
	require_once 'site-functions.php';



	$host = $_SERVER['HTTP_HOST'];
	$is_locally = ($host == 'localhost' ? true : false);
	$protocol = (stripos($_SERVER['SERVER_PROTOCOL'], 'http') !== false ? 'https' : 'http');
	$Parsedown = new Parsedown();

	$filename_path = explode('/', $_SERVER['PHP_SELF']);
	$filename = $filename_path[count($filename_path) - 1];
	$filename_query = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null;
	$filename_get = !empty($filename_query) ? $filename_query : '-';

	# Load the configuration file
	$config = json_decode(file_get_contents('../config.json'), false);

	$og_url_noprotocol = ($config->url->domain == null ? 'domain.com' : $config->url->domain);
	$og_url = $protocol.'://'.$og_url_noprotocol;
	$og_title = ($config->title == null ? '-' : $config->title);
	$og_description = 'The documentation for Serenum.';

	$arr_beginswith = [
		'<h', '<u', '<l', '<div', '</'
	];

?>
