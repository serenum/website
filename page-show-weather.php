<?php

	require_once 'site-header.php';

	function validateLatLong($lat, $long) {
		return preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $lat.','.$long);
	}



	$url = 'https://api.serenum.org/index.php?coor='.$get_coordinates.'&key='.$get_apikey;
	list($latitude, $longitude) = explode(',', $get_coordinates);

	$cache_file = $path_cache.'/'.md5(number_format($latitude, 2).','.number_format($longitude, 2)).'.cache';
	if(file_exists($cache_file)) {
		if(time() - filemtime($cache_file) > 3600) {
			unlink($cache_file);
			$cache = file_get_contents($url);
			file_put_contents($cache_file, $cache);
		}
	} else {
		$cache = file_get_contents($url);
		file_put_contents($cache_file, $cache);
	}

	$weather = json_decode(@file_get_contents($cache_file), false);

	$place_timezone = (empty($weather->timezone->timezone) ? null : $weather->timezone->timezone);
	$place_date = (empty($weather->datetime->local_timestamp) ? null : $weather->datetime->local_timestamp);
	$place_day = (empty($weather->datetime->details->day->weekday->full) ? null : $weather->datetime->details->day->weekday->full);
	$place_hour_24 = (empty($weather->datetime->local_time->hours->twentyfour) ? null : $weather->datetime->local_time->hours->twentyfour);
	$place_hour_12 = (empty($weather->datetime->local_time->hours->twelve) ? null : $weather->datetime->local_time->hours->twelve);
	$place_minutes = (empty($weather->datetime->local_time->minutes) ? null : $weather->datetime->local_time->minutes);
	$place_ampm = (empty($weather->datetime->local_time->ampm) ? null : $weather->datetime->local_time->ampm);

	$place_road = (empty($weather->place->road) ? null : $weather->place->road);
	$place_hamlet = (empty($weather->place->hamlet) ? null : $weather->place->hamlet);
	$place_village = (empty($weather->place->village) ? null : $weather->place->village);
	$place_town = (empty($weather->place->town) ? null : $weather->place->town);
	$place_city = (empty($weather->place->city) ? null : $weather->place->city);
	$place_state = (empty($weather->place->state) ? null : $weather->place->state);
	$place_countrycode = (empty($weather->place->country_code) ? null : $weather->place->country_code);
	$place_link = (empty($weather->place->link) ? null : $weather->place->link);

	$weather_name = (empty($weather->weather->current->weather->weather->description) ? null : $weather->weather->current->weather->weather->description);
	$weather_icon = (empty($weather->weather->current->weather->weather->icon) ? null : $weather->weather->current->weather->weather->icon);
	$weather_id = (empty($weather->weather->current->weather->weather->id) ? null : $weather->weather->current->weather->weather->id);
	$weather_idicon = (empty($weather->weather->current->weather->weather->id_icon) ? null : $weather->weather->current->weather->weather->id_icon);

	$weather_temp_celcius = (empty($weather->weather->current->weather->temp->celcius) ? null : $weather->weather->current->weather->temp->celcius);
	$weather_temp_fahrenheit = (empty($weather->weather->current->weather->temp->fahrenheit) ? null : $weather->weather->current->weather->temp->fahrenheit);
	$weather_temp_kelvin = (empty($weather->weather->current->weather->temp->kelvin) ? null : $weather->weather->current->weather->temp->kelvin);

	$weather_tempfl_celcius = (empty($weather->weather->current->weather->temp_feels_like->celcius) ? null : $weather->weather->current->weather->temp_feels_like->celcius);
	$weather_tempfl_fahrenheit = (empty($weather->weather->current->weather->temp_feels_like->fahrenheit) ? null : $weather->weather->current->weather->temp_feels_like->fahrenheit);
	$weather_tempfl_kelvin = (empty($weather->weather->current->weather->temp_feels_like->kelvin) ? null : $weather->weather->current->weather->temp_feels_like->kelvin);

	$weather_tempdp_celcius = (empty($weather->weather->current->weather->temp_dew_point->celcius) ? null : $weather->weather->current->weather->temp_dew_point->celcius);
	$weather_tempdp_fahrenheit = (empty($weather->weather->current->weather->temp_dew_point->fahrenheit) ? null : $weather->weather->current->weather->temp_dew_point->fahrenheit);
	$weather_tempdp_kelvin = (empty($weather->weather->current->weather->temp_dew_point->kelvin) ? null : $weather->weather->current->weather->temp_dew_point->kelvin);

	$weather_wind_ms = (empty($weather->weather->current->weather->wind->speed->ms->wind) ? null : $weather->weather->current->weather->wind->speed->ms->wind);
	$weather_wind_kmh = (empty($weather->weather->current->weather->wind->speed->kmh->wind) ? null : $weather->weather->current->weather->wind->speed->kmh->wind);
	$weather_wind_mph = (empty($weather->weather->current->weather->wind->speed->mph->wind) ? null : $weather->weather->current->weather->wind->speed->mph->wind);
	$weather_wind_knot = (empty($weather->weather->current->weather->wind->speed->knot->wind) ? null : $weather->weather->current->weather->wind->speed->knot->wind);
	$weather_wind_beaufort = (empty($weather->weather->current->weather->wind->beaufort) ? null : $weather->weather->current->weather->wind->beaufort);

	$weather_pressure_hpa = (empty($weather->weather->current->weather->pressure->hpa) ? null : $weather->weather->current->weather->pressure->hpa);
	$weather_pressure_pa = (empty($weather->weather->current->weather->pressure->pa) ? null : $weather->weather->current->weather->pressure->pa);
	$weather_pressure_bar = (empty($weather->weather->current->weather->pressure->bar) ? null : $weather->weather->current->weather->pressure->bar);
	$weather_pressure_mmhg = (empty($weather->weather->current->weather->pressure->mmhg) ? null : $weather->weather->current->weather->pressure->mmhg);
	$weather_pressure_inhg = (empty($weather->weather->current->weather->pressure->inhg) ? null : $weather->weather->current->weather->pressure->inhg);
	$weather_pressure_psi = (empty($weather->weather->current->weather->pressure->psi) ? null : $weather->weather->current->weather->pressure->psi);
	$weather_pressure_atm = (empty($weather->weather->current->weather->pressure->atm) ? null : $weather->weather->current->weather->pressure->atm);

	$weather_precip_rain_mm = (empty($weather->weather->current->weather->weather->rain->mm) ? 0 : $weather->weather->current->weather->weather->rain->mm);
	$weather_precip_rain_cm = (empty($weather->weather->current->weather->weather->rain->cm) ? 0 : $weather->weather->current->weather->weather->rain->cm);
	$weather_precip_rain_in = (empty($weather->weather->current->weather->weather->rain->in) ? 0 : $weather->weather->current->weather->weather->rain->in);

	$weather_precip_snow_mm = (empty($weather->weather->current->weather->weather->snow->mm) ? 0 : $weather->weather->current->weather->weather->snow->mm);
	$weather_precip_snow_cm = (empty($weather->weather->current->weather->weather->snow->cm) ? 0 : $weather->weather->current->weather->weather->snow->cm);
	$weather_precip_snow_in = (empty($weather->weather->current->weather->weather->snow->in) ? 0 : $weather->weather->current->weather->weather->snow->in);

	$weather_visibility_m = (empty($weather->weather->current->weather->visibility->m) ? null : $weather->weather->current->weather->visibility->m);
	$weather_visibility_km = (empty($weather->weather->current->weather->visibility->km) ? null : $weather->weather->current->weather->visibility->km);
	$weather_visibility_mi = (empty($weather->weather->current->weather->visibility->mi) ? null : $weather->weather->current->weather->visibility->mi);

	$weather_wind_direction_degrees = (empty($weather->weather->current->weather->wind->direction->degrees) ? null : $weather->weather->current->weather->wind->direction->degrees);
	$weather_wind_direction_compass = (empty($weather->weather->current->weather->wind->direction->compass) ? null : $weather->weather->current->weather->wind->direction->compass);
	$weather_clouds = (empty($weather->weather->current->weather->clouds) ? 0 : $weather->weather->current->weather->clouds);
	$weather_humidity = (empty($weather->weather->current->weather->humidity) ? 0 : $weather->weather->current->weather->humidity);
	$weather_h2o_kg_air = (empty($weather->weather->current->weather->h2o_per_kg_of_air->value) ? null : $weather->weather->current->weather->h2o_per_kg_of_air->value);
	$weather_uvindex = (empty($weather->weather->current->weather->uv_index->grade) ? 0 : $weather->weather->current->weather->uv_index->grade);
	$weather_uvindex_colour = (empty($weather->weather->current->weather->uv_index->colour) ? null : $weather->weather->current->weather->uv_index->colour);
	$weather_airquality_colour = (empty($weather->weather->current->air_pollution->quality->colour) ? null : $weather->weather->current->air_pollution->quality->colour);
	$weather_airquality_aqi = (empty($weather->weather->current->air_pollution->aqi) ? 0 : $weather->weather->current->air_pollution->aqi);

	$delimiter = (($get_decimal == null ? $default->settings->decimal : $get_decimal) == 0 ? ',' : '.');

	$arr_days = [
		"monday" => $lang->words->days->monday,
		"tuesday" => $lang->words->days->tuesday,
		"wednesday" => $lang->words->days->wednesday,
		"thursday" => $lang->words->days->thursday,
		"friday" => $lang->words->days->friday,
		"saturday" => $lang->words->days->saturday,
		"sunday" => $lang->words->days->sunday
	];

	$arr_place = [
		'road' => $place_road,
		'hamlet' => $place_hamlet,
		'village' => $place_village,
		'town' => $place_town,
		'city' => $place_city,
		'state' => $place_state,
		'country_code' => mb_strtoupper($place_countrycode)
	];

	$arr_timeformat = [
		0 => $place_hour_12.':'.(($place_minutes < 10 OR $place_minutes == 0) ? '0' : null) . $place_minutes.' '.$place_ampm,
		1 => (($place_hour_24 < 10 OR $place_hour_24 == 0) ? '0' : null) . $place_hour_24.':'.(($place_minutes < 10 OR $place_minutes == 0) ? '0' : null) . $place_minutes
	];

	$arr_temperature = [
		0 => $weather_temp_celcius,
		1 => $weather_temp_fahrenheit,
		2 => $weather_temp_kelvin
	];

	$arr_temperature_feelslike = [
		0 => $weather_tempfl_celcius,
		1 => $weather_tempfl_fahrenheit,
		2 => $weather_tempfl_kelvin
	];

	$arr_temperature_dewpoint = [
		0 => $weather_tempdp_celcius,
		1 => $weather_tempdp_fahrenheit,
		2 => $weather_tempdp_kelvin
	];

	$arr_wind_speed = [
		0 => $weather_wind_ms,
		1 => $weather_wind_kmh,
		2 => $weather_wind_mph,
		3 => $weather_wind_knot
	];

	$arr_pressure = [
		0 => format_number($weather_pressure_hpa, 0, $delimiter),
		1 => format_number($weather_pressure_pa, 3, $delimiter),
		2 => format_number($weather_pressure_bar, 3, $delimiter),
		3 => format_number($weather_pressure_mmhg, 0, $delimiter),
		4 => format_number($weather_pressure_inhg, 2, $delimiter),
		5 => format_number($weather_pressure_psi, 3, $delimiter),
		6 => format_number($weather_pressure_atm, 3, $delimiter)
	];

	$arr_precipitation_rain = [
		0 => $weather_precip_rain_cm,
		1 => $weather_precip_rain_cm,
		2 => $weather_precip_rain_in
	];

	$arr_precipitation_snow = [
		0 => $weather_precip_snow_mm,
		1 => $weather_precip_snow_cm,
		2 => $weather_precip_snow_in
	];

	$arr_distance = [
		0 => $weather_visibility_m,
		1 => $weather_visibility_km,
		2 => $weather_visibility_mi
	];



	$loop_minutely = (empty($weather->weather->minutely) ? null : $weather->weather->minutely);
	$loop_hourly = (empty($weather->weather->hourly) ? null : $weather->weather->hourly);
	$loop_daily = (empty($weather->weather->daily) ? null : $weather->weather->daily);







	echo '<section id="weather">';

		# Side panel
		require_once 'page-settings-options.php';


		echo '<div class="content">';

			# Check if data exists
			if(empty($get_coordinates)) {
				logger('The coordinates field was left empty.', 'error');

				echo '<div class="message color-red">';
					echo $lang->messages->no_coordinates;
				echo '</div>';



			/*} elseif(validateLatLong($get_latitude, $get_longitude)) {
				logger('Could not validate the coordinates.', 'error', [
					'coordinates' => $get_coordinates
				]);

				echo '<div class="message color-red">';
					echo $lang->messages->notvalid_coordinates;
				echo '</div>';*/



			} elseif($get_apikey != 'default' AND !preg_match('/([A-z0-9]){32}/i', $get_apikey)) {
				logger('Could not validate the API key for OpenWeatherMap.', 'error', [
					'apikey' => $get_apikey
				]);

				echo '<div class="message color-red">';
					echo $lang->messages->notvalid_apikey;
				echo '</div>';



			} elseif(empty($place_date)) {
				logger('Were not able to get weather information.', 'error', [
					'keys' => [
						'owm' => $get_apikey
					]
				]);

				echo '<div class="message color-blue">';
					echo $lang->messages->info_null;
				echo '</div>';





			} else {

				# Information about the place
				echo '<div class="placeinfo">';
					echo '<div>';
						echo date_($place_date, 'year-month-day').', '.$arr_timeformat[$get_timeformat];
					echo '</div>';

					echo '<div class="address">';
						echo '<div>';
							if(empty($arr_place['country_code'])) {
								echo '<span class="color-grey">'.$lang->messages->unknown_place.'</span>';
							} else {
								echo implode(svgicon('arrow-right'), array_filter($arr_place));
							}
						echo '</div>';

						echo '<div>';
							echo link_($lang->messages->open_in_osm, $place_link);
						echo '</div>';
					echo '</div>';
				echo '</div>';

				# Navigation
				echo '<nav>';
					echo '<a href="'.url('weather?pag=now', false).'"'.(($get_page == null OR $get_page == 'now') ? ' id="active"' : '').'>';
						echo $lang->menu->now;
					echo '</a>';

					echo '<a href="'.url('weather?pag=hourly', false).'"'.($get_page == 'hourly' ? ' id="active"' : '').'>';
						echo $lang->menu->hourly;
					echo '</a>';

					echo '<a href="'.url('weather?pag=daily', false).'"'.($get_page == 'daily' ? ' id="active"' : '').'>';
						echo $lang->menu->daily;
					echo '</a>';

					#echo '<a href="'.url('weather?pag=astronomy', false).'">';
						#echo $lang->menu->astronomy;
					#echo '</a>';
				echo '</nav>';



				if($get_page == 'hourly') {
					require_once 'page-hourly.php';

				} elseif($get_page == 'daily') {
					require_once 'page-daily.php';

				} else {
					require_once 'page-now.php';
				}

			}
		echo '</div>';

	echo '</section>';







	require_once 'site-footer.php';

?>
