<?php

	require_once 'site-header.php';

	logger('Loaded the contact page.');







	echo '<section id="contact">';
		echo '<h1>'.$lang->pages->contact->title.'</h1>';

		echo '<div>';
			foreach($config->contact AS $key => $value) {
				echo '<div>';
					echo '<div class="icon '.$key.'"></div>';
					echo '<div>';
						if(strpos($value, 'https://') !== false) {
							echo link_(str_replace('https://', '', $value), $value);
						} else {
							echo str_replace('@', ' at ', $value);
						}
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
