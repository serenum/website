<?php

				echo '</main>';



				echo '<footer>';
					echo '<div>';
						echo '<div>Serenum v'.$config->version.'</div>';
						echo '<div>'.$lang->footer->poweredby.' '.link_('Serenum API', 'https://api.serenum.org/wiki').'</div>';
					echo '</div>';


					echo '<nav>';
						echo '<a href="'.url('weather', false).'">';
							echo $lang->footer->nav->weather;
						echo '</a>';

						if(!isset($_GET['coo'])) {
							echo '<span class="color-grey">';
								echo $lang->footer->nav->save;
							echo '</span>';

						} else {
							echo '<a href="'.url('settings/save', false).'">';
								echo $lang->footer->nav->save;
							echo '</a>';
						}

						echo '<a href="'.url('settings/restore', false).'">';
							echo $lang->footer->nav->restore;
						echo '</a>';

						echo '<a href="'.url('', false).'">';
							echo $lang->footer->nav->intro;
						echo '</a>';

						echo '<a href="'.url('contact', false).'">';
							echo $lang->footer->nav->contact;
						echo '</a>';
					echo '</nav>';


					echo '<div class="icons">';
						echo '<div>Icons made by '.link_('Joel G.', 'https://joelchrono12.xyz/').' (logo and favicon included) and '.link_('Pe-icon-7-weather', 'https://themes-pixeden.com/font-demos/7-weather/index.html').'</div>';
					echo '</div>';
				echo '</footer>';
			echo '</section>';



		echo '</body>';
	echo '</html>';

?>