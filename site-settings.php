<?php

	require_once __DIR__.'/vendor/autoload.php';
	require_once __DIR__.'/site-functions.php';

	use Location\Coordinate;
	use Location\Formatter\Coordinate\DMS;
	use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;




	$db_host = 'localhost';
	$db_name = 'serenum';
	$db_username = null;
	$db_password = null;

	$settings_encryption_key = null;
	$settings_encryption_iv = null;

	$path_files = '/path/to/files';
	$path_cache = $path_files.'/cache';
	$path_logs = $path_files.'/logs';
	$path_images = 'images';
	$path_icons = $path_images.'/icons';

	$database_exists = false;
	$table_exists = false;

	$host = $_SERVER['HTTP_HOST'];
	$is_locally = ($host == 'localhost' ? true : false);
	$protocol = (stripos($_SERVER['SERVER_PROTOCOL'], 'http') !== false ? 'https' : 'http');
	$Parsedown = new Parsedown();

	$filename_path = explode('/', $_SERVER['PHP_SELF']);
	$filename = $filename_path[count($filename_path) - 1];
	$filename_query = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null;
	$filename_get = !empty($filename_query) ? $filename_query : '-';

	# Load the configuration file
	$config = json_decode(file_get_contents(__DIR__.'/config.json'), false);

	# Load the configuration file with default settings
	$default = json_decode(file_get_contents($path_files.'/defaults.json'), false);

	$get_page = (isset($_GET['pag']) ? strip_tags(htmlspecialchars($_GET['pag'])) : null);
	$get_coordinates = (isset($_GET['coo']) ? strip_tags(htmlspecialchars($_GET['coo'])) : (isset($post_coordinates) ? $post_coordinates : $default->coordinates->latitude.','.$default->coordinates->longitude));
	$get_language = (isset($_GET['lan']) ? strip_tags(htmlspecialchars($_GET['lan'])) : (isset($post_language) ? $post_language : $default->settings->language));
	$get_temperature = (isset($_GET['tem']) ? strip_tags(htmlspecialchars($_GET['tem'])) : (isset($post_settings) ? $post_settings[0] : $default->settings->temperature));
	$get_windspeed = (isset($_GET['wis']) ? strip_tags(htmlspecialchars($_GET['wis'])) : (isset($post_settings) ? $post_settings[1] : $default->settings->windspeed));
	$get_pressure = (isset($_GET['prs']) ? strip_tags(htmlspecialchars($_GET['prs'])) : (isset($post_settings) ? $post_settings[2] : $default->settings->pressure));
	$get_dateformat = (isset($_GET['dat']) ? strip_tags(htmlspecialchars($_GET['dat'])) : (isset($post_settings) ? $post_settings[3] : $default->settings->dateformat));
	$get_timeformat = (isset($_GET['tim']) ? strip_tags(htmlspecialchars($_GET['tim'])) : (isset($post_settings) ? $post_settings[5] : $default->settings->timeformat));
	$get_daysuffix = (isset($_GET['dsu']) ? strip_tags(htmlspecialchars($_GET['dsu'])) : (isset($post_settings) ? $post_settings[4] : $default->settings->daysuffix));
	$get_distance = (isset($_GET['dis']) ? strip_tags(htmlspecialchars($_GET['dis'])) : (isset($post_settings) ? $post_settings[6] : $default->settings->distance));
	$get_precipitation = (isset($_GET['prc']) ? strip_tags(htmlspecialchars($_GET['prc'])) : (isset($post_settings) ? $post_settings[7] : $default->settings->precipitation));
	$get_decimal = (isset($_GET['dec']) ? strip_tags(htmlspecialchars($_GET['dec'])) : (isset($post_settings) ? $post_settings[8] : $default->settings->decimal));
	$get_splitthousands = (isset($_GET['ths']) ? strip_tags(htmlspecialchars($_GET['ths'])) : (isset($post_settings) ? $post_settings[9] : $default->settings->split_thousands));
	$get_theme = (isset($_GET['thm']) ? strip_tags(htmlspecialchars($_GET['thm'])) : (isset($post_settings) ? $post_settings[10] : $default->settings->theme));
	$get_apikey = ((isset($_GET['api']) AND strip_tags(htmlspecialchars($_GET['api'])) != 'default') ? strip_tags(htmlspecialchars($_GET['api'])) : $default->apikey);
	$get_apikey_url = (isset($_GET['api']) ? strip_tags(htmlspecialchars($_GET['api'])) : (isset($post_apikey) ? $post_apikey : 'default'));

	if(!isset($_GET['coo']) AND isset($_GET['lan'])) {
		$get_url = 'lan='.$get_language;
	} elseif(isset($_GET['coo']) OR isset($post_coordinates)) {
		$get_url = 'coo='.$get_coordinates;
		$get_url .= '&lan='.$get_language;
		$get_url .= '&tem='.$get_temperature;
		$get_url .= '&wis='.$get_windspeed;
		$get_url .= '&prs='.$get_pressure;
		$get_url .= '&dat='.$get_dateformat;
		$get_url .= '&tim='.$get_timeformat;
		$get_url .= '&dsu='.$get_daysuffix;
		$get_url .= '&dis='.$get_distance;
		$get_url .= '&prc='.$get_precipitation;
		$get_url .= '&dec='.$get_decimal;
		$get_url .= '&ths='.$get_splitthousands;
		$get_url .= '&thm='.$get_theme;
		$get_url .= '&api='.$get_apikey_url;
	}

	# Load the configuration and language file
	$lang = json_decode(file_get_contents(__DIR__.'/languages/'.(isset($_GET['lan']) ? $get_language : $default->settings->language).'.json'), false);

	$og_url_noprotocol = $config->url->domain;
	$og_url = $protocol.'://'.$og_url_noprotocol;
	$og_title = ($config->title == null ? '-' : $config->title);
	$og_description = ($lang->metadata->description == null ? 'No description found.' : $lang->metadata->description);

	if(!empty($get_coordinates)) {
		list($get_latitude, $get_longitude) = explode(',', $get_coordinates);
	}

	$arr_beginswith = [
		'<h', '<u', '<l', '<div', '</'
	];



	$coordinate = new Coordinate($get_latitude, $get_longitude);
	$formatter = new DMS();
	$formatter->setSeparator(", ")->useCardinalLetters(true)->setUnits(DMS::UNITS_ASCII);



	try {
		$sql = new PDO('mysql:host='.$db_host, $db_username, $db_password);
		$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	catch(PDOException $e) {
		$config->save_to_db->enabled = false;
		/*echo $e;
		exit;*/
	}

	$ncrypt = new mukto90\Ncrypt;
	$ncrypt->set_secret_key($settings_encryption_key);
	$ncrypt->set_secret_iv($settings_encryption_iv);
	$ncrypt->set_cipher('AES-256-CBC');

	$generator = new ComputerPasswordGenerator();
	$generator->setUppercase()->setLowercase()->setNumbers()->setSymbols(false)->setLength(20);
	$password = $generator->generatePasswords(1);

	if($config->save_to_db->enabled == true) {
		$database_exists =
		sql("SELECT COUNT(SCHEMA_NAME)
			 FROM INFORMATION_SCHEMA.SCHEMATA
			 WHERE SCHEMA_NAME = '".$db_name."'
			", Array(), 'count');

		if($database_exists != 0) {
			$sql = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_username, $db_password);
			$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$table_exists =
			sql("SELECT COUNT(*)
				 FROM information_schema.TABLES
				 WHERE TABLE_SCHEMA = '".$db_name."'
				 AND TABLE_NAME = 'saved'
				", Array(), 'count');
		}
	}



	use MatthiasMullie\Minify;

	# Minify CSS files and JS files
	if($config->minify->enabled == true AND isset($_GET['minify'])) {
		$minifier = new Minify\CSS('css/desktop.css', 'css/portable.css');
		$minifier->minify('css/style.min.css');

		$minifier = new Minify\CSS('css/theme-auto.css');
		$minifier->minify('css/theme-auto.min.css');

		$minifier = new Minify\CSS('css/theme-light.css');
		$minifier->minify('css/theme-light.min.css');

		$minifier = new Minify\CSS('css/theme-dark.css');
		$minifier->minify('css/theme-dark.min.css');

		$minifier = new Minify\CSS('css/pe-icon-7-weather.css');
		$minifier->minify('css/pe-icon-7-weather.min.css');

		$minifier = new Minify\CSS('css/weather-icons.min.css', 'css/weather-icons-wind.css');
		$minifier->minify('css/weather-icons.min.css');

		$minifier = new Minify\CSS('css/leaflet.css', 'css/leaflet.fullscreen.css');
		$minifier->minify('css/leaflet.min.css');
		$minifier = new Minify\JS('js/leaflet.js', 'js/leaflet.fullscreen.min.js');
		$minifier->minify('js/leaflet.min.js');

		$minifier = new Minify\JS('js/swipe-events.js');
		$minifier->minify('js/swipe-events.min.js');

		$minifier = new Minify\JS('js/main.js');
		$minifier->minify('js/main.min.js');


		header("Location: ".url(''));
		exit;
	}



	create_folder($path_cache);
	create_folder($path_logs);

?>
