var folder_name = '/' + (window.location.host == 'serenum.org' ? '' : 'serenum');
var map = null;
var gps_options = {
	maximumAge: 0,
	timeout: 10000,
	enableHighAccuracy: false
};

function gps_success(p) {
	var latitude = p.coords.latitude.toFixed(3);
	var longitude = p.coords.longitude.toFixed(3);

	marker.setLatLng([latitude, longitude]);
	map.setView([latitude, longitude]);

	$('[name="field-coordinates"]').val(latitude + ',' + longitude);
	$('.start, .fetching, .error').hide();
	$('.found').show();
	$('[name="field-coordinates"]').attr({ 'readonly': false });
}

function gps_error(p) {
	var arr_codes = {
		1: 'permission-denied',
		2: 'position-unavailable',
		3: 'timeout'
	};

	$('.start, .found, .fetching').hide();
	$('.error.' + arr_codes[p.code]).show();
	$('[name="field-coordinates"]').attr({ 'readonly': false });
}

function showsettings() {
	var use_minified = $('main').data('useminified');
	var use_cdn = $('main').data('usecdn');
	var cdn_url = $('main').data('cdnurl');

	if($('section#weather').is(':visible')) {
		if(navigator.geolocation) {
			$('body').on('click', 'a.start, a.found', function() {
				$('.start, .found, .error').hide();
				$('.fetching').show();
				$('[name="field-coordinates"]').attr({ 'readonly': true });
				navigator.geolocation.getCurrentPosition(gps_success, gps_error, gps_options);
			});

			$('body').on('click', '.try-again', function() {
				$('.start, .found, .error').hide();
				$('.fetching').show();
				$('[name="field-coordinates"]').attr({ 'readonly': true });
				navigator.geolocation.getCurrentPosition(gps_success, gps_error, gps_options);
			});
		} else {
			$('.gps').hide();
		}
	}



	if(map == null) {
		var coordinates = $('[name="field-coordinates"]').val().split(',');
		var southWest = L.latLng(-89.98155760646617, -180), northEast = L.latLng(89.99346179538875, 180);
		var bounds = L.latLngBounds(southWest, northEast);

		var markericon = L.icon({
			iconUrl: ((use_minified == 'yes' && use_cdn == 'yes') ? cdn_url + '/img/marker.png' : folder_name + '/images/marker.png'),
			iconSize: [50, 50], 
			iconAnchor: [24, 45],
			popupAnchor: [-3, -76]
		});

		map = L.map('map', {
			zoom: 13,
			maxZoom: 17,
			minZoom: 3,
			zoomControl: false
		}).setView([coordinates[0], coordinates[1]]);

		marker = L.marker([coordinates[0], coordinates[1]], {
			icon: markericon,
			draggable: true,
			title: $('div#map').data('marker-drag')
		}).addTo(map);

		map.addControl(new L.Control.Fullscreen({
			title: {
				'false': $('div#map').data('fullscreen-open'),
				'true': $('div#map').data('fullscreen-close')
			}
		}));

		L.control.zoom({
			position: 'topleft',
			zoomInTitle: $('div#map').data('zoom-in'),
			zoomOutTitle: $('div#map').data('zoom-out')
		}).addTo(map);



		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

		map._onResize();
		map.setMaxBounds(bounds);
		map.on('drag', function() {
			map.panInsideBounds(bounds, { animate: false });
		});

		map.on('fullscreenchange', function() {
			if(map.isFullscreen()) {
				var flat = marker.getLatLng().lat;
				var flng = marker.getLatLng().lng;
				map.setView([flat, flng]);

			} else {
				var flat = marker.getLatLng().lat;
				var flng = marker.getLatLng().lng;
				map.setView([flat, flng]);
			}
		});


		map.on('click', function(c) {
			var clat = c.latlng.lat.toFixed(4);
			var clng = c.latlng.lng.toFixed(4);

			marker.setLatLng([clat, clng]);
			map.setView([clat, clng]);

			$('[name="field-coordinates"]').val(clat + ',' + clng);
		});

		marker.on('dragend', function() {
			var dlat = marker.getLatLng().lat.toFixed(4);
			var dlng = marker.getLatLng().lng.toFixed(4);

			marker.setLatLng([dlat, dlng]);
			map.setView([dlat, dlng]);

			$('[name="field-coordinates"]').val(dlat + ',' + dlng);
		});



		$('div#map > .leaflet-control-container > .leaflet-bottom.leaflet-right > div > a').attr({
			'target': '_blank'
		});
	}
}



$(document).ready(function () {


	/* Show the settings and initialize the map */
	$('body').on('click', 'details', function() {
		showsettings();
	});


	/* Keyboard shortcuts */
	/*if($('section#now').is(':visible')) {
		Mousetrap.bind('right', function() {
			window.location = folder_name + '/weather?pag=hourly';
		});

		$('main').on('swipeleft',  function() {
			window.location = folder_name + '/weather?pag=hourly';
		});


	} else if($('section#hourly').is(':visible')) {
		Mousetrap.bind('left', function() {
			window.location = folder_name + '/weather?pag=now';
		});

		Mousetrap.bind('right', function() {
			window.location = folder_name + '/weather?pag=daily';
		});

		$('main').on('swiperight',  function() {
			window.location = folder_name + '/weather?pag=now';
		});

		$('main').on('swipeleft',  function() {
			window.location = folder_name + '/weather?pag=daily';
		});


	} else if($('section#daily').is(':visible')) {
		Mousetrap.bind('left', function() {
			window.location = folder_name + '/weather?pag=hourly';
		});

		$('main').on('swiperight',  function() {
			window.location = folder_name + '/weather?pag=hourly';
		});
	}*/


	/*if($('section:not(#now)').is(':visible')) {
		Mousetrap.bind('n o w', function() {
			window.location = folder_name + '/weather?pag=now';
		});
	}

	if($('section:not(#hourly)').is(':visible')) {
		Mousetrap.bind('4 8 h', function() {
			window.location = folder_name + '/weather?pag=hourly';
		});
	}

	if($('section:not(#daily)').is(':visible')) {
		Mousetrap.bind('7 d', function() {
			window.location = folder_name + '/weather?pag=daily';
		});
	}



	Mousetrap.bind('s e t t i n g s', function() {
		$('details').attr({ 'open': ($('details').prop('open') === true ? false : true) });
		if($('details').prop('open') === true) {
			showsettings();

			Mousetrap.bind('g p s', function() {
				$('.start, .found, .error').hide();
				$('.fetching').show();
				$('[name="field-coordinates"]').attr({ 'readonly': true });
				navigator.geolocation.getCurrentPosition(gps_success, gps_error, gps_options);
			});

			Mousetrap.bind('s a v e', function() {
				$('form').submit();
			});
		}
	});*/


});