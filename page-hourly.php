<?php

	logger('Loaded the hourly weather forecast.');



	# Hourly forecast
	echo '<section id="hourly">';

		echo '<div class="loop">';
			$i = 0;
			foreach($loop_hourly AS $hourly) {
				$i++;
				$timestamp = $hourly->timestamp;
				$hour_24 = $hourly->time->hours->twentyfour;
				$hour_12 = $hourly->time->hours->twelve;
				$minutes = $hourly->time->minutes;
				$ampm = $hourly->time->ampm;
				$weather_h_id = $hourly->weather->id;
				$weather_h_idicon = $hourly->weather->id_icon;
				$weather_h_icon = $hourly->weather->icon;
				$weather_h_clouds = $hourly->clouds;
				$weather_h_humidity  = $hourly->humidity;
				$weather_h_h2okgair  = $hourly->h2o_per_kg_of_air->value;
				$weather_h_uvindex = $hourly->uv_index->grade;
				$weather_h_uvindex_colour = $hourly->uv_index->colour;
				$weather_h_precip_rain = $hourly->weather->rain->mm;
				$weather_h_precip_snow = $hourly->weather->snow->mm;

				$weather_h_pressure_hpa = $hourly->pressure->hpa;
				$weather_h_pressure_pa = $hourly->pressure->pa;
				$weather_h_pressure_bar = $hourly->pressure->bar;
				$weather_h_pressure_mmhg = $hourly->pressure->mmhg;
				$weather_h_pressure_inhg = $hourly->pressure->inhg;
				$weather_h_pressure_psi = $hourly->pressure->psi;
				$weather_h_pressure_atm = $hourly->pressure->atm;

				$weather_h_wind_ms = $hourly->wind->speed->ms->wind;
				$weather_h_wind_kmh = $hourly->wind->speed->kmh->wind;
				$weather_h_wind_mph = $hourly->wind->speed->mph->wind;
				$weather_h_wind_kt = $hourly->wind->speed->knot->wind;
				$weather_h_wind_direction_compass = $hourly->wind->direction->compass;
				$weather_h_wind_beaufort = $hourly->wind->beaufort;

				$weather_h_precip_rain_mm = $hourly->weather->rain->mm;
				$weather_h_precip_rain_cm = $hourly->weather->rain->cm;
				$weather_h_precip_rain_in = $hourly->weather->rain->in;

				$weather_h_precip_snow_mm = $hourly->weather->snow->mm;
				$weather_h_precip_snow_cm = $hourly->weather->snow->cm;
				$weather_h_precip_snow_in = $hourly->weather->snow->in;

				$weather_h_visibility_m = $hourly->visibility->m;
				$weather_h_visibility_km = $hourly->visibility->km;
				$weather_h_visibility_mi = $hourly->visibility->mi;

				$arr_timeformat = [
					0 => $hour_12.':'.(($minutes < 10 OR $minutes == 0) ? '0' : null) . $minutes.' '.$ampm,
					1 => (($hour_24 < 10 OR $hour_24 == 0) ? '0' : null) . $hour_24.':'.(($minutes < 10 OR $minutes == 0) ? '0' : null) . $minutes
				];

				$arr_temperature = [
					0 => $hourly->temperature->celcius,
					1 => $hourly->temperature->fahrenheit,
					2 => $hourly->temperature->kelvin
				];

				$arr_temperature_feelslike = [
					0 => $hourly->temperature_feels_like->celcius,
					1 => $hourly->temperature_feels_like->fahrenheit,
					2 => $hourly->temperature_feels_like->kelvin
				];

				$arr_wind_speed = [
					0 => $weather_h_wind_ms,
					1 => $weather_h_wind_kmh,
					2 => $weather_h_wind_mph,
					3 => $weather_h_wind_kt
				];

				$arr_pressure = [
					0 => format_number($weather_h_pressure_hpa, 0, $delimiter),
					1 => format_number($weather_h_pressure_pa, 3, $delimiter),
					2 => format_number($weather_h_pressure_bar, 3, $delimiter),
					3 => format_number($weather_h_pressure_mmhg, 0, $delimiter),
					4 => format_number($weather_h_pressure_inhg, 2, $delimiter),
					5 => format_number($weather_h_pressure_psi, 3, $delimiter),
					6 => format_number($weather_h_pressure_atm, 3, $delimiter)
				];

				$arr_precipitation_rain = [
					0 => $weather_h_precip_rain_mm,
					1 => $weather_h_precip_rain_cm,
					2 => $weather_h_precip_rain_in
				];

				$arr_precipitation_snow = [
					0 => $weather_h_precip_snow_mm,
					1 => $weather_h_precip_snow_cm,
					2 => $weather_h_precip_snow_in
				];

				$arr_distance = [
					0 => $weather_h_visibility_m,
					1 => $weather_h_visibility_km,
					2 => $weather_h_visibility_mi
				];


				echo '<div id="'.$timestamp.'">';
					echo '<div class="happens">';
						echo '<div class="day">';
							echo '<a href="#'.$timestamp.'">';
								echo humanreadable_days($timestamp, $place_timezone).', '.$arr_timeformat[$get_timeformat];
							echo '</a>';
						echo '</div>';
					echo '</div>';

					echo '<div class="weather">';
						echo weather($weather_h_idicon . $weather_h_id);
					echo '</div>';

					echo '<i class="'.$weather_h_icon.'"></i>';



					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->temperature->actual;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_temperature[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->temperature->feels_like;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_temperature_feelslike[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->wind->speed;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_wind_speed[$get_windspeed], 1, $delimiter).' '.get_type('windspeed', $get_windspeed);
						echo '</div>';
					echo '</div>';

					# Wind direction
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->wind->direction;
						echo '</div>';

						echo '<div class="value">';
							echo '<div class="string">';
								echo $weather_h_wind_direction_compass;
							echo '</div>';

							echo '<div class="icon"><i class="wi wi-wind wi-towards-'.mb_strtolower($weather_h_wind_direction_compass).'"></i></div>';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->wind->beaufort;
						echo '</div>';

						echo '<div class="value">';
							echo $weather_h_wind_beaufort;
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->clouds;
						echo '</div>';

						echo '<div class="value">';
							echo $weather_h_clouds.'%';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->humidity;
						echo '</div>';

						echo '<div class="value">';
							echo $weather_h_humidity.'%';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->h2o_kg_air;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($weather_h_h2okgair, 1, $delimiter).' '.mb_strtolower($lang->words->grams);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->pressure;
						echo '</div>';

						echo '<div class="value">';
							echo $arr_pressure[$get_pressure].' '.get_type('pressure', $get_pressure);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->uvindex;
						echo '</div>';

						echo '<div class="value">';
							echo $weather_h_uvindex.'<div class="dot colorbg-'.$weather_h_uvindex_colour.'"></div>';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->visibility;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_distance[$get_distance], 2, $delimiter).' '.get_type('distance', $get_distance);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->precipitation->rain->short;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_precipitation_rain[$get_precipitation], 2, $delimiter).' '.get_type('precipitation', $get_precipitation);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang->words->precipitation->snow->short;
						echo '</div>';

						echo '<div class="value">';
							echo format_number($arr_precipitation_snow[$get_precipitation], 2, $delimiter).' '.get_type('precipitation', $get_precipitation);
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';



		echo '<div class="charts">';

			echo '<div class="temp">';
				echo '<h4>'.$lang->words->temperature->actual.' (°'.get_type('temperature', $get_temperature).')</h4>';

				echo '<div>';
					echo '<div class="legend temp">';
						echo '<div>'.legend_temp(60, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(40, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(20, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(0, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-20, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-40, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-60, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-80, get_type('temperature', $get_temperature)).'</div>';
						echo '<div>'.legend_temp(-100, get_type('temperature', $get_temperature)).'</div>';
					echo '</div>';

					$it = 0;
					foreach($loop_hourly AS $hourly) {
						$it++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;

						$arr_temperature = [
							0 => $hourly->temperature->celcius,
							1 => $hourly->temperature->fahrenheit,
							2 => $hourly->temperature->kelvin
						];

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart temp">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.format_number($arr_temperature[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature).'">';
								echo '<div class="bars">';
									echo '<div class="chart-bar plus bar-'.$it.'">';
										echo '<div class="bar" style="height: '.(strpos($arr_temperature[$get_temperature], '-') !== false ? 0 : css_temp($arr_temperature[$get_temperature], get_type('temperature', $get_temperature))).';"></div>';
									echo '</div>';

									echo '<div class="chart-bar minus bar-'.$it.'">';
										echo '<div class="bar" style="height: '.(strpos($arr_temperature[$get_temperature], '-') === false ? 0 : css_temp($arr_temperature[$get_temperature], get_type('temperature', $get_temperature))).';"></div>';
									echo '</div>';
								echo '</div>';

								if($it % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';



			echo '<div>';
				echo '<h4>'.$lang->words->wind->speed.' ('.get_type('windspeed', $get_windspeed).')</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>'.legend_windspeed(100, get_type('windspeed', $get_windspeed)).'</div>';
						echo '<div>'.legend_windspeed(50, get_type('windspeed', $get_windspeed)).'</div>';
						echo '<div>'.legend_windspeed(0, get_type('windspeed', $get_windspeed)).'</div>';
					echo '</div>';

					$iw = 0;
					foreach($loop_hourly AS $hourly) {
						$iw++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;

						$arr_wind_speed = [
							0 => $hourly->wind->speed->ms->wind,
							1 => $hourly->wind->speed->kmh->wind,
							2 => $hourly->wind->speed->mph->wind,
							3 => $hourly->wind->speed->knot->wind
						];

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.format_number($arr_wind_speed[$get_windspeed], 1, $delimiter).' '.get_type('windspeed', $get_windspeed).'">';
								echo '<div class="chart-bar bar-'.$iw.'">';
									echo '<div class="bar" style="height: '.css_windspeed($arr_wind_speed[$get_windspeed], get_type('windspeed', $get_windspeed)).';"></div>';
								echo '</div>';

								if($iw % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';



			echo '<div>';
				echo '<h4>'.$lang->words->clouds.' (%)</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ic = 0;
					foreach($loop_hourly AS $hourly) {
						$ic++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;
						$weather_c_clouds = $hourly->clouds;

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.$weather_c_clouds.'%">';
								echo '<div class="chart-bar bar-'.$ic.'">';
									echo '<div class="bar" style="height: '.$weather_c_clouds.'%;"></div>';
								echo '</div>';

								if($ic % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';

			echo '<div>';
				echo '<h4>'.$lang->words->humidity.' (%)</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ih = 0;
					foreach($loop_hourly AS $hourly) {
						$ih++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;
						$weather_c_humidity = $hourly->humidity;

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.$weather_c_humidity.'%">';
								echo '<div class="chart-bar bar-'.$ih.'">';
									echo '<div class="bar" style="height: '.$weather_c_humidity.'%;"></div>';
								echo '</div>';

								if($ih % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';

			echo '<div>';
				echo '<h4>'.$lang->words->precipitation->rain->full.' ('.get_type('precipitation', $get_precipitation).')</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ipr = 0;
					foreach($loop_hourly AS $hourly) {
						$ipr++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;

						$arr_precipitation_rain = [
							0 => $hourly->weather->rain->mm,
							1 => $hourly->weather->rain->cm,
							2 => $hourly->weather->rain->in
						];

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.format_number($arr_precipitation_rain[$get_precipitation], 1, $delimiter).' '.get_type('precipitation', $get_precipitation).'">';
								echo '<div class="chart-bar bar-'.$ipr.'">';
									echo '<div class="bar" style="height: calc('.$arr_precipitation_rain[$get_precipitation].'% * 5);"></div>';
								echo '</div>';

								if($ipr % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';

			echo '<div>';
				echo '<h4>'.$lang->words->precipitation->snow->full.' ('.get_type('precipitation', $get_precipitation).')</h4>';

				echo '<div>';
					echo '<div class="legend">';
						echo '<div>100</div>';
						echo '<div>50</div>';
						echo '<div>0</div>';
					echo '</div>';

					$ips = 0;
					foreach($loop_hourly AS $hourly) {
						$ips++;
						$hour_h_timestamp = $hourly->timestamp;
						$hour_h_24 = $hourly->time->hours->twentyfour;
						$hour_h_12 = $hourly->time->hours->twelve;
						$hour_h_minutes = $hourly->time->minutes;
						$hour_h_ampm = $hourly->time->ampm;

						$arr_precipitation_snow = [
							0 => $hourly->weather->snow->mm,
							1 => $hourly->weather->snow->cm,
							2 => $hourly->weather->snow->in
						];

						$arr_timeformat = [
							0 => (($hour_h_12 < 10 OR $hour_h_12 == 0) ? '<span class="no-select transparent">0</span>' : null) . $hour_h_12.'<div class="ampm">'.$hour_h_ampm.'</div>',
							1 => (($hour_h_24 < 10 OR $hour_h_24 == 0) ? '0' : null) . $hour_h_24
						];


						echo '<div class="chart">';
							echo '<a href="#'.$hour_h_timestamp.'" data-tooltip="'.format_number($arr_precipitation_snow[$get_precipitation], 1, $delimiter).' °'.get_type('precipitation', $get_precipitation).'">';
								echo '<div class="chart-bar bar-'.$ips.'">';
									echo '<div class="bar" style="height: calc('.$arr_precipitation_snow[$get_precipitation].'% * 5);"></div>';
								echo '</div>';

								if($ips % 2) {
									echo '<div class="label">';
										echo $arr_timeformat[$get_timeformat];
									echo '</div>';
								}
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';

		echo '</div>';

	echo '</section>';

?>
