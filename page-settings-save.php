<?php

	if(isset($_GET['dl'])) {

		require_once 'site-settings.php';

		logger('Downloaded the settings as a JSON file.', 'debug');



		$coordinates = strip_tags(htmlspecialchars($_GET['coo']));
		$language = strip_tags(htmlspecialchars($_GET['lan']));
		$temperature = strip_tags(htmlspecialchars($_GET['tem']));
		$windspeed = strip_tags(htmlspecialchars($_GET['wis']));
		$pressure = strip_tags(htmlspecialchars($_GET['prs']));
		$dateformat = strip_tags(htmlspecialchars($_GET['dat']));
		$timeformat = strip_tags(htmlspecialchars($_GET['tim']));
		$daysuffix = strip_tags(htmlspecialchars($_GET['dsu']));
		$distance = strip_tags(htmlspecialchars($_GET['dis']));
		$precipitation = strip_tags(htmlspecialchars($_GET['prc']));
		$decimal = strip_tags(htmlspecialchars($_GET['dec']));
		$splitthousands = strip_tags(htmlspecialchars($_GET['ths']));
		$theme = strip_tags(htmlspecialchars($_GET['thm']));
		$apikey = strip_tags(htmlspecialchars($_GET['api']));
		$fname = 'Serenum - '.str_replace(',', ' ', $coordinates).' - '.date('Ymd-Hi');

		list($latitude, $longitude) = explode(',', $coordinates);

		$data = [
			'coordinates' => [
				'latitude' => $latitude,
				'longitude' => $longitude
			],
			'language' => $language,
			'options' => [
				'temperature' => $temperature,
				'wind_speed' => $windspeed,
				'pressure' => $pressure,
				'dateformat' => $dateformat,
				'timeformat' => $timeformat,
				'daysuffix' => $daysuffix,
				'distance' => $distance,
				'precipitation' => $precipitation,
				'decimal' => $decimal,
				'split_thousands' => $splitthousands,
				'theme' => $theme,
				'apikey' => $apikey
			],
			'template' => [
				'README' => 'The template shows what the digits have for value on serenum.org.',
				'temperature' => [ 'C', 'F', 'K' ],
				'wind_speed' => [ 'm/s', 'km/h', 'mph', 'kt' ],
				'pressure' => [ 'hPa', 'Pa', 'bar', 'mmHg', 'inHg', 'psi', 'atm' ],
				'dateformat' => [ 'MDY', 'DMY', 'YMD' ],
				'timeformat' => [ '12h', '24h' ],
				'daysuffix' => [ 'Enable', 'Disable' ],
				'distance' => [ 'm', 'km', 'mi' ],
				'precipitation' => [ 'mm', 'cm', 'in' ],
				'decimal' => [ 'Comma (,)', 'Dot (.)' ],
				'split_thousands' => [ 'Yes', 'No' ],
				'theme' => [ 'light', 'dark' ]
			]
		];

		$data = json_encode($data);

		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/force-download");
		header("Content-Type: application/download");
		header("Content-disposition: ".$fname.".json");
		header("Content-disposition: filename=".$fname.".json");

		print $data;
		exit;







	} else {

		require_once 'site-header.php';

		logger('Loaded the save settings page.');







		echo '<section id="save-settings">';
			echo '<h1>'.$lang->pages->settings->save->title.'</h1>';


			if($config->save_to_db->enabled != false OR $table_exists != 0) {
				echo '<div class="content database">';
					echo '<div><div class="icon download-database"></div></div>';

					echo '<div>';
						foreach($lang->pages->settings->save->database->content AS $database) {
							echo (in_array(substr($database, 0, 2), $arr_beginswith) ? $database : $Parsedown->text($database));
						}

						echo '<form action="'.url('forms/settings-save.php?met=db').'" method="POST">';
							echo '<input type="hidden" name="hidden-coordinates" value="'.$get_coordinates.'">';
							echo '<input type="hidden" name="hidden-language" value="'.$get_language.'">';
							echo '<input type="hidden" name="hidden-settings" value="'.$get_temperature . $get_windspeed . $get_pressure . $get_timeformat . $get_distance . $get_precipitation . $get_decimal . $get_splitthousands . $get_theme.'">';
							echo '<input type="hidden" name="hidden-apikey" value="'.$get_apikey_url.'">';

							echo '<input type="text" name="field-password" value="'.$password[0].'" readonly>';
							echo '<input type="submit" name="button-save" value="'.$lang->words->save.'">';
						echo '</form>';
					echo '</div>';
				echo '</div>';
			}



			echo '<div class="content json">';
				echo '<div><div class="icon download-json"></div></div>';

				echo '<div>';
					foreach($lang->pages->settings->save->json->content AS $json) {
						echo (in_array(substr($json, 0, 2), $arr_beginswith) ? $json : $Parsedown->text($json));
					}

					echo '<div class="download">';
						echo '<a href="'.url('settings/download?dl&coo='.$get_coordinates.'&lan='.$get_language.'&tem='.$get_temperature.'&wis='.$get_windspeed.'&prs='.$get_pressure.'&dat='.$get_dateformat.'&tim='.$get_timeformat.'&dsu='.$get_daysuffix.'&dis='.$get_distance.'&prc='.$get_precipitation.'&dec='.$get_decimal.'&ths='.$get_splitthousands.'&thm='.$get_theme.'&api='.$get_apikey_url, true).'">';
							echo '<div class="icon download"></div>';
							echo $lang->words->download;
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</div>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
