<?php

	$get_method = (isset($_GET['met']) ? strip_tags(htmlspecialchars($_GET['met'])) : null);
	$get_db = ($get_method == 'db' ? true : false);
	$get_json = ($get_method == 'json' ? true : false);

	$post_password = ($get_db == true ? strip_tags(htmlspecialchars($_POST['field-password'])) : null);


	require_once '../site-settings.php';


	if($get_db == true) {
		if(empty($post_password)) {
			logger('Tried to restore the settings with no password.', 'error');
			header("Location: ".url('settings/restore?met=db&err=1'));
			exit;

		} else {
			$check_password =
			sql("SELECT COUNT(data_password)
				 FROM saved
				 WHERE data_password = :_password
				", Array(
					'_password' => MD5($post_password)
				), 'count');

			if($check_password == 0) {
				logger('Were not able to restore the settings due to incorrect password.', 'error', [
					'password' => $post_password,
					'hash' => MD5($post_password)
				]);

				header("Location: ".url('settings/restore?met=db&err=2'));
				exit;


			} else {
				$settings =
				sql("SELECT id,
							settings_coordinates,
							settings_language,
							settings_options,
							settings_apikey,
							timestamp_accessed

					 FROM saved
					 WHERE data_password = :_password
					", Array(
						'_password' => MD5($post_password)
					), 'fetch');

				logger('Restored the settings from the database.', 'debug', [
					'last_accessed' => date('Y-m-d, H:i:s', endecrypt($settings['timestamp_accessed'], false))
				]);


				$coordinates = endecrypt($settings['settings_coordinates'], false);
				$language = endecrypt($settings['settings_language'], false);
				$options = endecrypt($settings['settings_options'], false);
				$apikey = endecrypt($settings['settings_apikey'], false);

				sql("UPDATE saved
						SET timestamp_accessed = :_accessed
						WHERE id = :_iddata
					", Array(
						'_iddata' => (int)$settings['id'],
						'_accessed' => endecrypt(time())
					));
			}


			header("Location: ".url('weather?coo='.$coordinates.'&lan='.$language.'&tem='.$options[0].'&wis='.$options[1].'&prs='.$options[2].'&tim='.$options[3].'&dis='.$options[4].'&prc='.$options[5].'&dec='.$options[6].'&ths='.$options[7].'&thm='.$options[8].'&owm='.(empty($apikey) ? 'default' : $apikey), true));
			exit;
		}



	} elseif($get_json == true) {
		$fileinfo = pathinfo($_FILES['file']['name']);

		if($_FILES['file']['error'] != 0) {
			header("Location: ".url('settings/restore?met=json&err=1'));
			exit;

		} elseif(!is_uploaded_file($_FILES['file']['tmp_name'])) {
			header("Location: ".url('settings/restore?met=json&err=2'));
			exit;

		} elseif($_FILES['file']['size'] > (1024 * 1024 * 1)) {
			header("Location: ".url('settings/restore?met=json&err=3&siz='.$_FILES['file']['size']));
			exit;

		} elseif($fileinfo['extension'] != 'json') {
			header("Location: ".url('settings/restore?met=json&err=4&for='.$fileinfo['extension']));
			exit;


		} else {
			$content = file_get_contents($_FILES['file']['tmp_name']);

			if(!is_json($content)) {
				header("Location: ".url('settings/restore?met=json&err=5'));
				exit;

			} else {
				logger('Restored the settings from a JSON file.', 'debug');

				$content = json_decode($content, false);
				$coordinates = $content->coordinates->latitude.','.$content->coordinates->longitude;
				$language = $content->language;
				$temperature = $content->options->temperature;
				$windspeed = $content->options->wind_speed;
				$pressure = $content->options->pressure;
				$dateformat = $content->options->dateformat;
				$timeformat = $content->options->timeformat;
				$daysuffix = $content->options->daysuffix;
				$distance = $content->options->distance;
				$precipitation = $content->options->precipitation;
				$decimal = $content->options->decimal;
				$splitthousands = $content->options->split_thousands;
				$theme = $content->options->theme;
				$apikey = $content->options->apikey;

				header("Location: ".url('weather?coo='.$coordinates.'&lan='.$language.'&tem='.$temperature.'&wis='.$windspeed.'&prs='.$pressure.'&dat='.$dateformat.'&tim='.$timeformat.'&dsu='.$daysuffix.'&dis='.$distance.'&prc='.$precipitation.'&dec='.$decimal.'&ths='.$splitthousands.'&thm='.$theme.'&api='.(empty($apikey) ? 'default' : $apikey), true));
				exit;
			}
		}



	} else {
		die('error');
	}

?>
