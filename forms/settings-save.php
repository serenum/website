<?php

	$post_password = strip_tags(htmlspecialchars($_POST['field-password']));
	$post_coordinates = strip_tags(htmlspecialchars($_POST['hidden-coordinates']));
	$post_language = strip_tags(htmlspecialchars($_POST['hidden-language']));
	$post_settings = strip_tags(htmlspecialchars($_POST['hidden-settings']));
	$post_apikey = strip_tags(htmlspecialchars($_POST['hidden-apikey']));

	require_once '../site-settings.php';

	logger('Saved the settings to the database.', 'debug');



	sql("INSERT INTO saved(
			 data_password,
			 settings_coordinates,
			 settings_language,
			 settings_options,
			 settings_apikey,
			 timestamp_created
		  )

		  VALUES(
			  :_password,
			  :_coordinates,
			  :_language,
			  :_options,
			  :_apikey,
			  :_created
		  )
		 ", Array(
			 '_password' => MD5($post_password),
			 '_coordinates' => endecrypt($post_coordinates),
			 '_language' => endecrypt($post_language),
			 '_options' => endecrypt($post_settings),
			 '_apikey' => ($post_apikey == 'default' ? null : endecrypt($post_apikey)),
			 '_created' => endecrypt(time())
		), 'insert');



	header("Location: ".url('settings/save', false));
	exit;

?>
