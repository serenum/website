<?php

	function url($string, $pure = true, $docs = false) {
		global $is_locally, $get_url, $host;

		$get_dir = basename(__DIR__) . (($pure == false AND $string == '') ? '' : '/');
		$get_host = ($host == 'serenum.org' ? '' : basename(__DIR__).'/');
		$get_localstatus = ($is_locally == true ? $get_dir : '') . ($docs == false ? null : 'docs/');
		$get_pureness = ($pure == true ? $string : (empty($string) ? '' : $string) . ($get_url == null ? null : (strpos($string, '?') !== false ? '&' : '?')) . ltrim($get_url, '?'));

		return '/'.$get_localstatus . $get_pureness;
	}



	function link_($string, $link) {
		return '<a href="'.$link.'" target="_blank" rel="noopener noreferrer">'.$string.'</a>';
	}

	function endecrypt($string, $encrypt = true) {
		global $ncrypt;
		return ($encrypt == true ? $ncrypt->encrypt($string) : $ncrypt->decrypt($string));
	}

	function create_folder($string) {
		if(!file_exists($string)) {
			$oldmask = umask(0);
			mkdir($string, 0777, true);
			umask($oldmask);
		}
	}

	function is_json($string){
		return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	function humanreadable_days($string, $timezone) {
		global $lang, $arr_days, $date_weekday;

		$date_str = date('Y-m-d', $string);
		$date = new DateTime('now', new DateTimezone($timezone));

		if($date_str == $date->format('Y-m-d')) {
			return $lang->words->days->humanreadable->today;

		} elseif($date_str == $date->modify('+1 day')->format('Y-m-d')) {
			return $lang->words->days->humanreadable->day1_ahead;

		} else {
			$weekday = new DateTime($date_str, new DateTimezone($timezone));
			return $arr_days[mb_strtolower($weekday->format('l'))];
		}
	}

	function legend_temp($string, $type) {
		$arr_c = [
			60 => 60,
			40 => 40,
			20 => 20,
			0 => 0,
			-20 => -20,
			-40 => -40,
			-60 => -60,
			-80 => -80,
			-100 => -100
		];

		$arr_f = [
			60 => 140,
			40 => 104,
			20 => 68,
			0 => 32,
			-20 => -4,
			-40 => -40,
			-60 => -76,
			-80 => -112,
			-100 => -148
		];

		$arr_k = [
			60 => 333.15,
			40 => 313.15,
			20 => 293.15,
			0 => 273.15,
			-20 => -253.15,
			-40 => -233.15,
			-60 => -213.15,
			-80 => -193.15,
			-100 => -173.15
		];

		if($type == 'C') {
			return $arr_c[$string];
		} elseif($type == 'F') {
			return $arr_f[$string];
		} elseif($type == 'K') {
			return $arr_k[$string];
		}
	}

	function legend_windspeed($string, $type) {
		$arr_ms = [100 => 100, 50 => 50, 0 => 0];
		$arr_kmh = [100 => 360, 50 => 180, 0 => 0];
		$arr_mph = [100 => 223.7, 50 => 111.8, 0 => 0];
		$arr_kt = [100 => 194.4, 50 => 97.2, 0 => 0];

		if($type == 'm/s') {
			return $arr_ms[$string];
		} elseif($type == 'km/h') {
			return $arr_kmh[$string];
		} elseif($type == 'mph') {
			return $arr_mph[$string];
		} elseif($type == 'kt') {
			return $arr_kt[$string];
		}
	}

	function css_temp($string, $type) {
		if($type == 'C') {
			return 'calc('.$string.'% * 1)';
		} elseif($type == 'F') {
			return 'calc('.$string.'% / 4)';
		} elseif($type == 'K') {
			return 'calc('.$string.'% * 5)';
		}
	}

	function css_windspeed($string, $type) {
		if($type == 'm/s') {
			return 'calc('.$string.'% * 1)';
		} elseif($type == 'km/h') {
			return $string.'%';
		} elseif($type == 'mph') {
			return 'calc('.$string.'% * 5)';
		} elseif($type == 'kt') {
			return 'calc('.$string.'% * 5)';
		}
	}

	function forecastcharts($variable, $count, $multiply) {
		echo '<div class="chart">';
			echo '<div class="bar-'.$count.'">';
				echo '<a href="#'.$count.'">';
					echo '<div class="ghost"></div>';
					echo '<div class="bar" style="height: calc('.$variable.'% * '.$multiply.');"></div>';
				echo '</a>';
			echo '</div>';
		echo '</div>';
	}

	function svgicon($string) {
		$arr = [
			'settings' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M12,15.5A3.5,3.5 0 0,1 8.5,12A3.5,3.5 0 0,1 12,8.5A3.5,3.5 0 0,1 15.5,12A3.5,3.5 0 0,1 12,15.5M19.43,12.97C19.47,12.65 19.5,12.33 19.5,12C19.5,11.67 19.47,11.34 19.43,11L21.54,9.37C21.73,9.22 21.78,8.95 21.66,8.73L19.66,5.27C19.54,5.05 19.27,4.96 19.05,5.05L16.56,6.05C16.04,5.66 15.5,5.32 14.87,5.07L14.5,2.42C14.46,2.18 14.25,2 14,2H10C9.75,2 9.54,2.18 9.5,2.42L9.13,5.07C8.5,5.32 7.96,5.66 7.44,6.05L4.95,5.05C4.73,4.96 4.46,5.05 4.34,5.27L2.34,8.73C2.21,8.95 2.27,9.22 2.46,9.37L4.57,11C4.53,11.34 4.5,11.67 4.5,12C4.5,12.33 4.53,12.65 4.57,12.97L2.46,14.63C2.27,14.78 2.21,15.05 2.34,15.27L4.34,18.73C4.46,18.95 4.73,19.03 4.95,18.95L7.44,17.94C7.96,18.34 8.5,18.68 9.13,18.93L9.5,21.58C9.54,21.82 9.75,22 10,22H14C14.25,22 14.46,21.82 14.5,21.58L14.87,18.93C15.5,18.67 16.04,18.34 16.56,17.94L19.05,18.95C19.27,19.03 19.54,18.95 19.66,18.73L21.66,15.27C21.78,15.05 21.73,14.78 21.54,14.63L19.43,12.97Z" /></svg>',
			'winddir' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13 7.828V20h-2V7.828l-5.364 5.364-1.414-1.414L12 4l7.778 7.778-1.414 1.414L13 7.828z"/></svg>',
			'arrow-right' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z"/></svg>',
			'save' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 3h14l2.707 2.707a1 1 0 0 1 .293.707V20a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm3 1v5h9V4H7zm-1 8v7h12v-7H6zm7-7h2v3h-2V5z"/></svg>',
			'gps-found' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M12,8A4,4 0 0,1 16,12A4,4 0 0,1 12,16A4,4 0 0,1 8,12A4,4 0 0,1 12,8M3.05,13H1V11H3.05C3.5,6.83 6.83,3.5 11,3.05V1H13V3.05C17.17,3.5 20.5,6.83 20.95,11H23V13H20.95C20.5,17.17 17.17,20.5 13,20.95V23H11V20.95C6.83,20.5 3.5,17.17 3.05,13M12,5A7,7 0 0,0 5,12A7,7 0 0,0 12,19A7,7 0 0,0 19,12A7,7 0 0,0 12,5Z" /></svg>',
			'gps-start' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M3.05,13H1V11H3.05C3.5,6.83 6.83,3.5 11,3.05V1H13V3.05C17.17,3.5 20.5,6.83 20.95,11H23V13H20.95C20.5,17.17 17.17,20.5 13,20.95V23H11V20.95C6.83,20.5 3.5,17.17 3.05,13M12,5A7,7 0 0,0 5,12A7,7 0 0,0 12,19A7,7 0 0,0 19,12A7,7 0 0,0 12,5Z" /></svg>',
			'gps-fetching' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M3.05 13H1V11H3.05C3.5 6.83 6.83 3.5 11 3.05V1H13V3.05C17.17 3.5 20.5 6.83 20.95 11H23V13H20.95C20.5 17.17 17.17 20.5 13 20.95V23H11V20.95C6.83 20.5 3.5 17.17 3.05 13M12 5C8.13 5 5 8.13 5 12S8.13 19 12 19 19 15.87 19 12 15.87 5 12 5M11.13 17.25H12.88V15.5H11.13V17.25M12 6.75C10.07 6.75 8.5 8.32 8.5 10.25H10.25C10.25 9.28 11.03 8.5 12 8.5S13.75 9.28 13.75 10.25C13.75 12 11.13 11.78 11.13 14.63H12.88C12.88 12.66 15.5 12.44 15.5 10.25C15.5 8.32 13.93 6.75 12 6.75Z" /></svg>',
			'error' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-1-5h2v2h-2v-2zm0-8h2v6h-2V7z"/></svg>',
			'database' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" /></svg>',
			'json' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M12,3C8.59,3 5.69,4.07 4.54,5.57L9.79,10.82C10.5,10.93 11.22,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M3.92,7.08L2.5,8.5L5,11H0V13H5L2.5,15.5L3.92,16.92L8.84,12M20,9C20,11.21 16.42,13 12,13C11.34,13 10.7,12.95 10.09,12.87L7.62,15.34C8.88,15.75 10.38,16 12,16C16.42,16 20,14.21 20,12M20,14C20,16.21 16.42,18 12,18C9.72,18 7.67,17.5 6.21,16.75L4.53,18.43C5.68,19.93 8.59,21 12,21C16.42,21 20,19.21 20,17" /></svg>',
			'email' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M20,8L12,13L4,8V6L12,11L20,6M20,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V6C22,4.89 21.1,4 20,4Z" /></svg>',
			'telegram' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z"/></svg>',
			'map' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M15.5,12C18,12 20,14 20,16.5C20,17.38 19.75,18.21 19.31,18.9L22.39,22L21,23.39L17.88,20.32C17.19,20.75 16.37,21 15.5,21C13,21 11,19 11,16.5C11,14 13,12 15.5,12M15.5,14A2.5,2.5 0 0,0 13,16.5A2.5,2.5 0 0,0 15.5,19A2.5,2.5 0 0,0 18,16.5A2.5,2.5 0 0,0 15.5,14M14,6.11L8,4V15.89L9,16.24V16.5C9,17.14 9.09,17.76 9.26,18.34L8,17.9L2.66,19.97L2.5,20A0.5,0.5 0 0,1 2,19.5V4.38C2,4.15 2.15,3.97 2.36,3.9L8,2L14,4.1L19.34,2H19.5A0.5,0.5 0 0,1 20,2.5V11.81C18.83,10.69 17.25,10 15.5,10C15,10 14.5,10.06 14,10.17V6.11Z" /></svg>',
			'options' => '<svg viewBox="0 0 24 24"><path fill="currentColor" d="M3,17V19H9V17H3M3,5V7H13V5H3M13,21V19H21V17H13V15H11V21H13M7,9V11H3V13H7V15H9V9H7M21,13V11H11V13H21M15,9H17V7H21V5H17V3H15V9Z" /></svg>'
		];

		return $arr[$string];
	}

	function format_number($string, $decimals = 2, $delimiter = null) {
		global $default, $get_splitthousands;

		$string = number_format($string, $decimals, ($delimiter == null ? $default->settings->decimal : $delimiter), ($get_splitthousands == 0 ? ' ' : ''));
		$explode = explode($delimiter, $string);

		return $explode[0] . (!isset($explode[1]) ? null : ($explode[1] == 0 ? null : $delimiter . $explode[1]));
	}

	function get_type($string, $value) {
		global $default;

		if($string == 'temperature') {
			$arr = [
				0 => 'C',
				1 => 'F',
				2 => 'K'
			];


		} elseif($string == 'windspeed') {
			$arr = [
				0 => 'm/s',
				1 => 'km/h',
				2 => 'mph',
				3 => 'kt'
			];


		} elseif($string == 'pressure') {
			$arr = [
				0 => 'hpa',
				1 => 'pa',
				2 => 'bar',
				3 => 'mmhg',
				4 => 'inhg',
				5 => 'psi',
				6 => 'atm'
			];


		} elseif($string == 'distance') {
			$arr = [
				0 => 'm',
				1 => 'km',
				2 => 'mi'
			];


		} elseif($string == 'precipitation') {
			$arr = [
				0 => 'mm',
				1 => 'cm',
				2 => 'in'
			];
		}

		return $arr[$value];
	}



	function logger($string, $type = 'info', $error = null) {
		global $filename, $filename_get, $config, $path_logs;

		if($config->analytics == true) {
			create_folder($path_logs.'/'.date('Y-m-d'));

			$fileinfo = pathinfo($filename);
			$file = $path_logs.'/'.date('Y-m-d').'/'.$type.'__'.$fileinfo['filename'].'.txt';
			$content = date('Y-m-d, H:i:s').' - '.$filename . ((strpos($filename_get, 'coo') !== false OR $filename_get == '-') ? null : '?'.$filename_get).' - '.$type.' - '.(empty($error) ? $string : rtrim($string, '.').', '.json_encode($error, JSON_PRETTY_PRINT));

			file_put_contents($file, $content."\r\n", FILE_APPEND | LOCK_EX);
		}
	}



	function date_($timestamp, $format) {
		global $get_dateformat, $get_daysuffix, $lang;

		$months = Array(
			1 => $lang->words->months->january->full,
			2 => $lang->words->months->february->full,
			3 => $lang->words->months->march->full,
			4 => $lang->words->months->april->full,
			5 => $lang->words->months->may->full,
			6 => $lang->words->months->june->full,
			7 => $lang->words->months->july->full,
			8 => $lang->words->months->august->full,
			9 => $lang->words->months->september->full,
			10 => $lang->words->months->october->full,
			11 => $lang->words->months->november->full,
			12 => $lang->words->months->december->full
		);

		$months_short = Array(
			1 => $lang->words->months->january->short,
			2 => $lang->words->months->february->short,
			3 => $lang->words->months->march->short,
			4 => $lang->words->months->april->short,
			5 => $lang->words->months->may->short,
			6 => $lang->words->months->june->short,
			7 => $lang->words->months->july->short,
			8 => $lang->words->months->august->short,
			9 => $lang->words->months->september->short,
			10 => $lang->words->months->october->short,
			11 => $lang->words->months->november->short,
			12 => $lang->words->months->december->short
		);


		if($format == 'short') {
			return date('Y-m-d, H:i', $timestamp);

		} elseif($format == 'short-with-seconds') {
			return date('Y-m-d, H:i:s', $timestamp);

		} elseif($format == 'month-only') {
			return $months[$timestamp];

		} elseif($format == 'month-only-short') {
			return $months_short[$timestamp];

		} elseif($format == 'year-month') {
			return mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp);

		} elseif($format == 'year-month-day') {
			if($get_dateformat == 0) {
				return $months[date('n', $timestamp)].' '.date('j'.($get_daysuffix == 0 ? 'S' : ''), $timestamp).' '.date('Y', $timestamp);
			} elseif($get_dateformat == 1) {
				return date('j'.($get_daysuffix == 0 ? 'S' : ''), $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).' '.date('Y', $timestamp);
			}

		} elseif($format == 'month-day') {
			if($get_dateformat == 0) {
				return $months[date('n', $timestamp)].' '.date('j'.($get_daysuffix == 0 ? 'S' : ''), $timestamp);
			} elseif($get_dateformat == 1) {
				return date('j'.($get_daysuffix == 0 ? 'S' : ''), $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]);
			}

		} elseif($format == 'detailed') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp).', kl. '.date('H:i', $timestamp);
		}
	}



	# Open up configs/default-language.php to see the names of the weather codes.
	function weather($string) {
		global $lang;

		$arr = Array(
			'01d800' => $lang->weather->sunny,
			'01n800' => $lang->weather->moon,

			'02d801' => $lang->weather->clouds->few,
			'02n801' => $lang->weather->clouds->few,

			'03d802' => $lang->weather->clouds->scattered,
			'03n802' => $lang->weather->clouds->scattered,

			'04d803' => $lang->weather->clouds->broken,
			'04n803' => $lang->weather->clouds->broken,
			'04d804' => $lang->weather->clouds->overcast,
			'04n804' => $lang->weather->clouds->overcast,

			'09d300' => $lang->weather->drizzle->light_intensity,
			'09n300' => $lang->weather->drizzle->light_intensity,
			'09d301' => $lang->weather->drizzle->drizzle,
			'09n301' => $lang->weather->drizzle->drizzle,
			'09d302' => $lang->weather->drizzle->heavy_intensity,
			'09n302' => $lang->weather->drizzle->heavy_intensity,
			'09d310' => $lang->weather->drizzle->light_intensity_rain,
			'09n310' => $lang->weather->drizzle->light_intensity_rain,
			'09d311' => $lang->weather->drizzle->rain,
			'09n311' => $lang->weather->drizzle->rain,
			'09d312' => $lang->weather->drizzle->heavy_intensity_rain,
			'09n312' => $lang->weather->drizzle->heavy_intensity_rain,
			'09d313' => $lang->weather->drizzle->shower_rain,
			'09n313' => $lang->weather->drizzle->shower_rain,
			'09d314' => $lang->weather->drizzle->heavy_shower_rain,
			'09n314' => $lang->weather->drizzle->heavy_shower_rain,
			'09d321' => $lang->weather->drizzle->shower,
			'09n321' => $lang->weather->drizzle->shower,
			'09d520' => $lang->weather->rain->light_intensity_shower,
			'09n520' => $lang->weather->rain->light_intensity_shower,
			'09d521' => $lang->weather->rain->shower,
			'09n521' => $lang->weather->rain->shower,
			'09d522' => $lang->weather->rain->heavy_intensity_shower,
			'09n522' => $lang->weather->rain->heavy_intensity_shower,
			'09d531' => $lang->weather->rain->ragged_shower,
			'09n531' => $lang->weather->rain->ragged_shower,

			'10d500' => $lang->weather->rain->light,
			'10n500' => $lang->weather->rain->light,
			'10d501' => $lang->weather->rain->moderate,
			'10n501' => $lang->weather->rain->moderate,
			'10d502' => $lang->weather->rain->heavy_intensity,
			'10n502' => $lang->weather->rain->heavy_intensity,
			'10d503' => $lang->weather->rain->very_heavy,
			'10n503' => $lang->weather->rain->very_heavy,
			'10d504' => $lang->weather->rain->extreme,
			'10n504' => $lang->weather->rain->extreme,

			'11d200' => $lang->weather->thunderstorm->light_rain,
			'11n200' => $lang->weather->thunderstorm->light_rain,
			'11d201' => $lang->weather->thunderstorm->rain,
			'11n201' => $lang->weather->thunderstorm->rain,
			'11d202' => $lang->weather->thunderstorm->heavy_rain,
			'11n202' => $lang->weather->thunderstorm->heavy_rain,
			'11d210' => $lang->weather->thunderstorm->light,
			'11n210' => $lang->weather->thunderstorm->light,
			'11d211' => $lang->weather->thunderstorm->thunderstorm,
			'11n211' => $lang->weather->thunderstorm->thunderstorm,
			'11d212' => $lang->weather->thunderstorm->heavy,
			'11n212' => $lang->weather->thunderstorm->heavy,
			'11d221' => $lang->weather->thunderstorm->ragged,
			'11n221' => $lang->weather->thunderstorm->ragged,
			'11d230' => $lang->weather->thunderstorm->light_drizzle,
			'11n230' => $lang->weather->thunderstorm->light_drizzle,
			'11d231' => $lang->weather->thunderstorm->drizzle,
			'11n231' => $lang->weather->thunderstorm->drizzle,
			'11d232' => $lang->weather->thunderstorm->heavy_drizzle,
			'11n232' => $lang->weather->thunderstorm->heavy_drizzle,

			'13d511' => 'rain',
			'13d600' => 'snow-alt',
			'13d601' => 'snow-alt',
			'13d602' => 'snow-alt',
			'13d611' => 'snow',
			'13d612' => 'snow',
			'13d613' => 'snow',
			'13d615' => 'snow',
			'13d616' => 'snow',
			'13d620' => 'snow',
			'13d621' => 'snow',
			'13d622' => 'snow',
			'13n511' => 'rain',
			'13n600' => 'snow-alt',
			'13n601' => 'snow-alt',
			'13n602' => 'snow-alt',
			'13n611' => 'snow',
			'13n612' => 'snow',
			'13n613' => 'snow',
			'13n615' => 'snow',
			'13n616' => 'snow',
			'13n620' => 'snow',
			'13n621' => 'snow',
			'13n622' => 'snow',

			'50d701' => 'fog',
			'50d711' => 'fog',
			'50d721' => 'fog',
			'50d731' => 'fog',
			'50d741' => 'fog',
			'50d751' => 'fog',
			'50d761' => 'fog',
			'50d762' => 'fog',
			'50d771' => 'fog',
			'50d781' => 'hurricane',
			'50n701' => 'fog',
			'50n711' => 'fog',
			'50n721' => 'fog',
			'50n731' => 'fog',
			'50n741' => 'fog',
			'50n751' => 'fog',
			'50n761' => 'fog',
			'50n762' => 'fog',
			'50n771' => 'fog',
			'50n781' => 'hurricane'
		);

		return $arr[$string];
	}



	function sql($query, $array, $method = '') {

		global $sql;


		try {
			$prepare = $sql->prepare($query);

			if($method == 'count') {
				$prepare->execute($array);
				$data = $prepare->fetchColumn();
				return $data;


			} elseif($method == 'fetch') {
				$prepare->execute($array);
				$data = $prepare->fetch(PDO::FETCH_ASSOC);
				return $data;


			} elseif($method == 'insert') {
				foreach($array AS $data => $value) {
					$prepare->bindValue(':'.$data, $value);
				}

				$prepare->execute();
				return $value;


			} else {
				$prepare->execute($array);
				return $prepare;
			}


		} catch(Exception $e) {
			throw($e);
		}

	}

?>
