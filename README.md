Serenum is a weather service that focus on privacy. It currently supports the current weather and the forecast for 48 hours ahead and 7 days ahead. Serenum uses its own API to fetch the data from OpenWeatherMap.

You can easily install Serenum on your own server. Please go to [serenum.org/docs/installation](https://serenum.org/docs/installation) for more information.

[Serenum API](https://api.serenum.org) are [open sourced](https://codeberg.org/serenum/api) as well and can also be installed on your server, if you want to run everything local. Currently, the documentation for Serenum API can only be found at [api.serenum.org/wiki](https://api.serenum.org/wiki). The documentation will be moved to serenum.org/docs/api as soon as it is done.