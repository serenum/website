<?php

	logger('Loaded the current weather report.');



	# Weather image
	/*echo '<div class="cover">';
		echo '<img src="'.url('images/weather-backgrounds/sunny.webp').'">';
	echo '</div>';*/

	echo '<section id="now">';

		# Current
		echo '<div class="current">';
			echo '<div>';

				# Weather icon
				echo '<div class="icon">';
					echo '<i class="'.$weather_icon.'"></i>';
				echo '</div>';


				# Short information
				echo '<div class="shortinfo">';

					# Temperature
					echo '<div class="temperature">';
						echo format_number($arr_temperature[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
					echo '</div>';

					# Feels like temperature
					echo '<div class="feels-like">';
						echo $lang->words->temperature->feels_like.' '.format_number($arr_temperature_feelslike[$get_temperature], 1, $delimiter).' °'.get_type('temperature', $get_temperature);
					echo '</div>';

					# Weather
					echo '<div class="weather">';
						echo weather($weather_idicon . $weather_id);
					echo '</div>';

				echo '</div>';

			echo '</div>';
		echo '</div>';



		echo '<div class="current-details">';

			# Wind speed
			echo '<div>';
				echo '<div class="label">'.$lang->words->wind->speed.'</div>';
				echo '<div class="value">'.format_number($arr_wind_speed[$get_windspeed], 1, $delimiter).'</div>';
				echo '<div class="type">'.get_type('windspeed', $get_windspeed).'</div>';
			echo '</div>';

			# Wind direction
			echo '<div>';
				echo '<div class="label">'.$lang->words->wind->direction.'</div>';
				echo '<div class="value">';
					echo '<div class="string">';
						echo $weather_wind_direction_compass;
					echo '</div>';

					echo '<div class="icon"><i class="wi wi-wind wi-towards-'.mb_strtolower($weather_wind_direction_compass).'"></i></div>';
				echo '</div>';
				echo '<div class="type"></div>';
			echo '</div>';

			# Beaufort
			echo '<div>';
				echo '<div class="label">'.$lang->words->wind->beaufort.'</div>';
				echo '<div class="value">'.$weather_wind_beaufort.'</div>';
				echo '<div class="type"></div>';
			echo '</div>';


			# Clouds
			echo '<div>';
				echo '<div class="label">'.$lang->words->clouds.'</div>';
				echo '<div class="value">'.$weather_clouds.'</div>';
				echo '<div class="type">%</div>';
			echo '</div>';


			# Humidity
			echo '<div>';
				echo '<div class="label">'.$lang->words->humidity.'</div>';
				echo '<div class="value">'.$weather_humidity.'</div>';
				echo '<div class="type">%</div>';
			echo '</div>';


			# h2o per kg of air
			echo '<div>';
				echo '<div class="label">'.$lang->words->h2o_kg_air.'</div>';
				echo '<div class="value">'.format_number($weather_h2o_kg_air, 1, $delimiter).'</div>';
				echo '<div class="type">gram</div>';
			echo '</div>';


			# Pressure
			echo '<div>';
				echo '<div class="label">'.$lang->words->pressure.'</div>';
				echo '<div class="value">'.$arr_pressure[$get_pressure].'</div>';
				echo '<div class="type">'.get_type('pressure', $get_pressure).'</div>';
			echo '</div>';


			# Precipitation (rain)
			echo '<div>';
				echo '<div class="label">'.$lang->words->precipitation->rain->full.'</div>';
				echo '<div class="value">'.$arr_precipitation_rain[$get_precipitation].'</div>';
				echo '<div class="type">'.get_type('precipitation', $get_precipitation).'</div>';
			echo '</div>';


			# Precipitation (snow)
			echo '<div>';
				echo '<div class="label">'.$lang->words->precipitation->snow->full.'</div>';
				echo '<div class="value">'.$arr_precipitation_snow[$get_precipitation].'</div>';
				echo '<div class="type">'.get_type('precipitation', $get_precipitation).'</div>';
			echo '</div>';


			# Temperature (dew point)
			echo '<div>';
				echo '<div class="label">'.$lang->words->temperature->dew_point.'</div>';
				echo '<div class="value">'.format_number($arr_temperature_dewpoint[$get_temperature], 1, $delimiter).'</div>';
				echo '<div class="type">°'.get_type('temperature', $get_temperature).'</div>';
			echo '</div>';


			# Visibility
			echo '<div>';
				echo '<div class="label">'.$lang->words->visibility.'</div>';
				echo '<div class="value">'.format_number($arr_distance[$get_distance], 2, $delimiter).'</div>';
				echo '<div class="type">'.get_type('distance', $get_distance).'</div>';
			echo '</div>';


			# UV Index
			echo '<div>';
				echo '<div class="label">'.$lang->words->uvindex.'</div>';
				echo '<div class="value">'.$weather_uvindex.'<div class="dot colorbg-'.$weather_uvindex_colour.'"></div></div>';
				echo '<div class="type"></div>';
			echo '</div>';


			# Air quality
			echo '<div>';
				echo '<div class="label">'.$lang->words->airquality.'</div>';
				echo '<div class="value">'.$weather_airquality_aqi.'<div class="dot colorbg-'.$weather_airquality_colour.'"></div></div>';
				echo '<div class="type">AQI</div>';
			echo '</div>';

		echo '</div>';





		# Minutely forecast
		if($loop_minutely != null) {
			echo '<div class="minutely">';
				echo '<h3>'.$lang->titles->weather->forecasts->minutely.'</h3>';

				echo '<div class="loop">';
					$i = 0;
					foreach($loop_minutely AS $minutely) {
						$i++;
						$hour_24 = $minutely->time->hours->twentyfour;
						$hour_12 = $minutely->time->hours->twelve;
						$minutes = $minutely->time->minutes;
						$ampm = $minutely->time->ampm;
						$precip_mm = $minutely->precipitation->mm;
						$precip_cm = $minutely->precipitation->cm;
						$precip_in = $minutely->precipitation->in;

						$arr_timeformat = [
							0 => $hour_12.':'.(($minutes < 10 OR $minutes == 0) ? '0' : null) . $minutes.'<div class="ampm">'.$ampm.'</div>',
							1 => (($hour_24 < 10 OR $hour_24 == 0) ? '0' : null) . $hour_24.':'.(($minutes < 10 OR $minutes == 0) ? '0' : null) . $minutes
						];

						$arr_precipitation = [
							0 => $precip_mm,
							1 => $precip_cm,
							2 => $precip_in
						];


						echo '<div>';
							echo '<div class="chart">';
								echo '<div class="chart-bar bar-'.$i.'">';
									echo '<div class="ghost"></div>';
									echo '<div class="bar" style="height: calc('.$arr_precipitation[$get_precipitation].'% * 5);"></div>';
								echo '</div>';
							echo '</div>';

							echo '<div class="text mono">'.format_number($arr_precipitation[$get_precipitation], 1, $delimiter).'</div>';

							echo '<div class="time">';
								echo $arr_timeformat[$get_timeformat];
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';
		}

	echo '</section>';

?>
